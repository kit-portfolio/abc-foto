# ABC foto

# Team

* Lead:
    * Mykola Mizhigurskiy;
* Developers:
    * Alexander Kairtsev;
    * Mykhailo Koval;
    * Sviatoslav Malyshevskyi;
* Designer:
    * Anton Konikov;
* Supervisor:
    * Roman Marchenko;

# Tech stack

* TypeScript;
* React;
* React Router;
* React Redux;
* Styled Components;
* GitLab CI/CD;
* ESLint (Google preset);
* ESDoc (TypeScript preset);
* Jest;

# Requirements

### Quality standards

Project is regulated
by [system requirements](https://gitlab.com/kit-portfolio/abc-foto/-/requirements_management/requirements). Compliance is
mandatory.  
Project is Git Flow compliant.

### Design mockup

Figma design mockup hosted [here](https://www.figma.com/file/D9kq90mdLvFYcrJlJlW1ZS/ABC-Photo-Prototype?node-id=0%3A1).

# Misc

### DEMO

Project demo is deployed [here](https://abc-foto-client.herokuapp.com/)

### Launching project
To launch project locally follow procedure:
1. Open **server** folder in the terminal.
   * Run `yarn database:start`
   * Run `yarn server:start`
   * Run `yarn database:seed`
2. Open **client** folder in the terminal.
   * Run `yarn start`
    
> **FYI:**  
> There are several dummy users in db. 
> 
> Logins: `jdoe@example.com`, `bpitt@example.com`, `jalba@example.com`, `jbrown@example.com`, `cshepard@example.com`.  
> Password for all users: `password`.  
> 
> CShepard user has orders.
