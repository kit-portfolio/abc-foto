import React from 'react';
import {Route, Redirect} from 'react-router';
import {useSelector} from 'react-redux';
import {TODO_ANY} from '../types/misc';
import Profile from '../components/main/profile/profile';

interface IProfileRouteProps {
  exact: boolean,
  path: string,
}
/**
 * User profile protected route
 * @returns JSX.Element
 */
export default function ProfileRoute(props: IProfileRouteProps): JSX.Element {
  const state = useSelector((state: TODO_ANY) => state.auth);
  return (
    <Route
      exact={props.exact}
      path={props.path}
      render={() => {
        if (state.isLoggedIn) {
          return <Profile/>;
        } else {
          return <Redirect to='/'/>;
        }
      }}/>
  );
}

