// MODULES
import React from 'react';
import {Switch} from 'react-router';
import {Route} from 'react-router-dom';
// COMPONENTS
import Homepage from '../components/main/homepage/homepage';
import Checkout from '../components/main/checkout/checkout';
import ProfileRoute from './profile-route';
import Catalog from '../components/main/catalog/catalog';
import Page404 from '../components/misc/page-404/page-404';
// HELPERS
import {URN} from '../config/constants';

/**
 * React Router routes controller
 * @returns JSX.Element
 */
export default function Routes():JSX.Element {
  return (
    <Switch>
      <Route exact path={URN.HOMEPAGE} component={Homepage}/>
      <Route exact path={URN.CHECKOUT} component={Checkout}/>
      <Route path="/catalog" component={Catalog}/>
      <ProfileRoute exact path={URN.PROFILE}/>
      <Route component={Page404}/>
    </Switch>
  );
}
