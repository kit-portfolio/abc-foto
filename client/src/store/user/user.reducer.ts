import {
  USER_ACTION,
  IUserAction,
} from './user.actions';

const initialState = {};

const reducer = (state = initialState, action: IUserAction) => {
  switch (action.type) {
    case USER_ACTION.FETCH: return action.payload;
    case USER_ACTION.WRITE: return action.payload;
    case USER_ACTION.FORGET: return {};
    default: return state;
  }
};

export default reducer;
