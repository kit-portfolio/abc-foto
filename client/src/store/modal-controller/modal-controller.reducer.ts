import {TODO_ANY} from '../../types/misc';
import {MODAL_ACTION} from './modal-controller.actions';

export interface IModalControllerAction {
  type: MODAL_ACTION,
  payload?: TODO_ANY,
}

export interface IModalController {
  type: MODAL_ACTION,
  payload?: TODO_ANY,
}

const initialState = {
  type: MODAL_ACTION.CLOSE_MODAL,
  config: undefined,
};

const reducer = (state = initialState, action: IModalControllerAction) => {
  switch (action.type) {
    case MODAL_ACTION.OPEN_AUTH_MODAL:
      return {
        type: action.type,
        config: undefined,
      };
    case MODAL_ACTION.OPEN_INFO_MODAL:
      return {
        type: action.type,
        config: action.payload,
      };
    case MODAL_ACTION.CLOSE_MODAL:
      return {};
    default:
      return state;
  }
};

export default reducer;
