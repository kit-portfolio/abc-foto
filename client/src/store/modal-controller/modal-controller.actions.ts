import {IInfoModalProps} from '../../components/misc/modals/info-modal/info-modal';

/**
 * @hint Do not dispatch actions directly. Use config below.
 */
export enum MODAL_ACTION {
  OPEN_AUTH_MODAL = 'OPEN AUTH MODAL',
  OPEN_INFO_MODAL = 'OPEN INFO MODAL',
  CLOSE_MODAL = 'CLOSE MODAL',
}

/**
 * Open authorisation modal action
 * @returns redux action object
 */
export function openAuthModal() {
  return {type: MODAL_ACTION.OPEN_AUTH_MODAL};
}

/**
 * Open informational modal action
 * @param config - informational modal contents
 * @returns redux action object
 */
export function openInfoModal(config: IInfoModalProps) {
  return {type: MODAL_ACTION.OPEN_INFO_MODAL, payload: config};
}

/**
 * Close modal action
 * @returns redux action object
 */
export function closeModal() {
  return {type: MODAL_ACTION.CLOSE_MODAL};
}
