import {ENDPOINT, LS_KEYS} from '../../config/constants';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
import {createWriteUserAction, USER_ACTION} from '../user/user.actions';
import {toast} from 'material-react-toastify';
import {TODO_ANY} from '../../types/misc';
import {env} from '../../config/environment';

/**
 * @hint Do not dispatch actions directly. Use config below.
 */
export enum AUTH_ACTION {
  LOG_IN = 'LOG IN',
  LOG_OUT = 'LOG OUT',
}

interface ITokenPayload {
  id: string,
  firstName: string,
  lastName: string,
  isAdmin: boolean
  exp: number,
}

/**
 * Helper function intended to handle log in process.
 * @returns void
 */
export function logIn(dispatch: TODO_ANY, token: string | null = localStorage.getItem(LS_KEYS.AUTH)): void {
  if (!token) {
    dispatch({type: AUTH_ACTION.LOG_OUT});
    return;
  }

  try {
    const authData: ITokenPayload = jwtDecode(token.substring(7));
    if (authData.exp * 1000 < Date.now()) {
      dispatch({type: AUTH_ACTION.LOG_OUT});
    } else {
      dispatch({type: AUTH_ACTION.LOG_IN, payload: {
        isLoggedIn: true,
        id: authData.id,
        exp: authData.exp,
        token,
      }});
    }
  } catch (e) {
    localStorage.removeItem(LS_KEYS.AUTH);
    dispatch({type: AUTH_ACTION.LOG_OUT});
  }

  axios.get(`${env.apiBaseUrl}${ENDPOINT.USERS}`, {
    headers: {'Authorization': token},
  })
      .then((res: TODO_ANY) => {
        dispatch(createWriteUserAction(res.data));
        localStorage.setItem(LS_KEYS.AUTH, token);
        toast.success(`Welcome, ${res.data.firstName}!`);
      })
      .catch((e) => {
        console.log(e);
        localStorage.removeItem(LS_KEYS.AUTH);
        dispatch({type: AUTH_ACTION.LOG_OUT});
        toast.error('Login failed');
      });
}


/**
 * Helper function intended to handle log out process.
 * @returns void
 */
export function logOut(dispatch: TODO_ANY): void {
  dispatch({type: AUTH_ACTION.LOG_OUT});
  dispatch({type: USER_ACTION.FORGET});
  localStorage.removeItem(LS_KEYS.AUTH);
}
