export enum PALETTE {
  WHITE = 'white',
  BLACK = 'black',
  PALE_SKY = '#677283',
  NOBEL = '#B7B7B7',
  LIGHT_GRAY = '#F7F5F6',
  APPLE = '#51AD33',
  ALIZARIN_CRIMSON = '#DF2725',
  DARK_RED = 'darkred',
  SKY_BLUE = '#00B0FF',
}

export enum FONT_SIZE {
  HEADER = 'font-size: 26px',
  SUB_HEADER = 'font-size: 20px',
  TEXT = 'font-size: 16px',
  SMALL_TEXT = 'font-size: 12px'
}
