/**
* @hint This type intended to be used as a placeholder for complex typing situations.
**/
export type TODO_ANY = any;

/**
* @hint Interface for shopping cart item.
**/
export interface ICartItem {
  itemId : string,
  quantity: number
}

/**
 * @hint Universal interface for typing basic static links
 **/
export interface IStaticLink {
  id : string,
  title : string,
  href : string
}

/**
 * @hint Interface for sale-announce announcements displayed on a main page
 **/
export interface ISaleAnnounce {
  id : string,
  image : string,
  desc : string,
  href : string
}

/**
 * @hint Footer static links are displayed in groups. This interface defines all properties for it.
 **/
export interface IFooterStaticLinksBlock {
  id : string,
  title : string,
  links : Array<IStaticLink>
}

/**
 * @hint Interface for order shipping options
 **/
export interface IShippingOption {
  id : string,
  address : string,
  Label : string,
  cost: number
}
