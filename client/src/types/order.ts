export enum ORDER_STATUS {
  PENDING = 'Pending',
  COMPLETED = 'Completed',
  CANCELLED = 'Cancelled'
}

/**
 * @hint `orderId` is not an DB-shape id, - it's detached entity, and it has it's own shape.
 */
export interface IOrder {
  orderId: string,
  date: number,
  comment: string,
  total: number,
  products: Array<string>,
  status: ORDER_STATUS,
  client: IClient,
  shipping: IShipping,
}

export enum SHIPPING_TYPE {
  PICKUP = 'Pickup',
  NOVA_POSCHTA = 'Nova Poschta',
  COURIER = 'Courier'
}

export interface IShipping {
  type: SHIPPING_TYPE,
  address: string,
  price: number
}

/**
 * @hint 'id' is optional because if order was made by non-authorised user, there will be no user id.
 */
export interface IClient {
  id?: string,
  name: string,
  phone: string,
  email: string
}
