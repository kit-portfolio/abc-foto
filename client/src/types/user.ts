export interface IUser {
  avatarUrl?: string
  firstName: string,
  lastName?: string,
  phone: string,
  email: string,
  birthDate?: string,
  gender?: sex
}

export type sex = 'Male' | 'Female';
