export default interface ICover{
  link: string,
  href: string,
  alt: string,
  _id: string,
};
