export interface IProduct {
  name: string,
  manufacturer: string,
  images: IImage[],
  description: string,
  quantity: number,
  type: string,
  category: string,
  code: string,
  price: string,
  accessories?: Array<string>,
  characteristics: Array<ICharacteristic>,
  sale?: ISale,
  _id: string,
}

interface ICharacteristic {
  title: string,
  value: string,
  isFilterable: boolean,
}

interface ISale {
  discount: number,
  endDate: string,
}

export interface IImage {
  _id: string,
  image: string,
  preview: string,
}

/**
 * @hint Used as placeholder until product page loads item info.
 */
export const emptyProduct: IProduct = {
  name: '-',
  manufacturer: '-',
  images: [{
    _id: '-',
    image: '-',
    preview: '-',
  }],
  description: '-',
  quantity: 0,
  type: '-',
  category: '-',
  code: '-',
  price: '-',
  characteristics: [{
    title: '-',
    value: '-',
    isFilterable: false,
  }],
  _id: '-',
};
