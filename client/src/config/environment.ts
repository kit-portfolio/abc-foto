interface IEnvironment {
  readonly apiBaseUrl: string,
}
const dev: IEnvironment = {
  apiBaseUrl: 'http://localhost:5000/api',
};
const prod: IEnvironment = {
  apiBaseUrl: '',
};
export const env = (process.env.REACT_APP_ENV === 'production') ? prod : dev;
