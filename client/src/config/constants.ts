/**
 * @hint Local storage keys.
 */
export enum LS_KEYS {
  AUTH = 'auth',
  LANGUAGE = 'lang'
}

export enum URN {
  CATALOG = '/catalog',
  CART = '/cart',
  CHECKOUT = '/checkout',
  HOMEPAGE = '/',
  PROFILE = '/profile'
}

export enum ENDPOINT {
  COVER = '/cover',
  LOGIN = '/users/login',
  ORDERS = '/orders',
  PASSWORD = '/users/password',
  PRODUCTS = '/products',
  SUBSCRIPTION = '/subscription',
  USERS = '/users',
}

export enum PRODUCT_CATEGORY {
  PHOTO = 'photo',
  MEMORY_CARDS = 'memory cards',
}

export enum PRODUCT_TYPES {
  LENS = 'lens',
  CAMERA = 'camera',
  PAPER = 'paper',
}
