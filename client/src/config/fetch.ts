import {Dispatch, SetStateAction} from 'react';
import axios from 'axios';

interface IFetchDataProps {
  href: string,
  setIsLoaded: Dispatch<SetStateAction<boolean>>,
  setItems: Dispatch<SetStateAction<any>>,
  shuffle?: boolean,
}

/**
 * Helper function intended to fetch data from a provided API-endpoint.
 * @returns void
 */
export function fetchData(props: IFetchDataProps): void {
  const {href, setIsLoaded, setItems, shuffle} = props;
  axios.get(href)
      .then(
          (result: any) => {
            if (shuffle) {
              result.data = arrayShuffle(result.data);
            }
            setIsLoaded(true);
            setItems(result.data);
          },
      );
}

/**
 * Fisher-Yates Shuffle
 * @returns array of product
 */
function arrayShuffle(arr: any[]) : any[] {
  let j;
  let temp;
  for (let i = arr.length - 1; i > 0; i--) {
    j = Math.floor(Math.random()*(i + 1));
    temp = arr[j];
    arr[j] = arr[i];
    arr[i] = temp;
  }
  return arr;
}
