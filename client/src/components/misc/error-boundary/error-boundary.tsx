import React from 'react';
import {TODO_ANY} from '../../../types/misc';
import ErrorPage from '../error-page/error-page';
import {Redirect} from 'react-router';
// @ts-ignore
import {ReactComponent as Error} from '../../../resources/error.svg';

interface IMyState {
  hasError: boolean,
  time: number,
}

/**
 * ErrorBoundary - catches errors at the stage of rendering.
 * @prop hasError - set to `true`, when error was captured.
 * @prop time - A property intended to store time left to homepage redirect.
 * @remarks Renders a page asking to restart the page or return to home.
 */
export default class ErrorBoundary extends React.Component<TODO_ANY, IMyState> {
  /**
   * Create state which indicates an error
   */
  constructor(props: TODO_ANY) {
    super(props);
    this.state = {
      hasError: false,
      time: 5,
    };
  }
  /**
   *Start timer on component load
   * @returns void
   */
  componentDidMount(): void {
    this.startTimer();
  }

  /**
   *Timer that changes state.time every second.
   * @returns void
   */
  startTimer(): void {
    const timer = setInterval(() => {
      this.setState({
        time: this.state.time - 1,
      });
      if (this.state.time === 0 ) {
        clearInterval(timer);
      }
    }, 1000);
  }
  /**
   * Default method to catch errors
   */
  static getDerivedStateFromError() {
    return {hasError: true};
  }

  /**
   * Render ErrorBoundary if hasError = true
   */
  render() {
    if (!this.state.hasError) {
      return this.props.children;
    }
    if (this.state.time === 0) {
      return <Redirect to='/'/>;
    }
    return (
      <ErrorPage
        header={'Что-то пошло не так'}
        message={`Произошла ошибка. Возращаем домой через ${this.state.time}`}
        image={<Error/>} />
    );
  }
}
