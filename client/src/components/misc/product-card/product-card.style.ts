import styled, {css} from 'styled-components';
import {PALETTE, FONT_SIZE} from '../../../theme/theme';
// @ts-ignore
import {ReactComponent as CartIcon} from '../../../resources/cart.svg';

export const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  cursor: pointer;
  
  &:hover {
    transition: 0.3s;
    box-shadow: 0 0 5px 1px ${PALETTE.APPLE};
  }
  
`;

interface IPhotoProps {
  image: string,
}

export const Photo = styled.div<IPhotoProps>`
  height: 170px;
  background-image: url(${(props) => props.image});
  background-repeat: no-repeat;
  background-size: 75%;
  background-position: top;
`;

export const Name = styled.p`
  ${FONT_SIZE.TEXT};
  font-weight: 700;
  height: 36px;
  text-transform: uppercase;
`;

export const Price = styled.p`
  ${FONT_SIZE.SUB_HEADER};
  font-weight: 700;
  grid-area: price;
`;

interface ICartProps {
  stockStatus: boolean,
}

export const Cart = styled(CartIcon)<ICartProps>`
  width: 35px;
  height: 35px;
  grid-area: cart;
  justify-self: flex-end;
  align-self: flex-start;
  
  & g {
    fill: ${PALETTE.APPLE};
  }
  
  .background-square{
    fill: ${PALETTE.WHITE}
  }
  
  ${(props) => props.stockStatus && css`

    &:hover  .background-square{
      fill: ${PALETTE.APPLE};
      transition: 0.6s;
    }
    
    &:hover g {
      fill: ${PALETTE.WHITE};
      transition: 0.2s;
    }
  `}
 
`;

interface IStockInfoProps {
  stockStatus?: boolean
}

export const StockInfo = styled.p<IStockInfoProps>`
  grid-area: stock;
  ${FONT_SIZE.TEXT};
  color: ${(props) => props.stockStatus ? PALETTE.APPLE : PALETTE.NOBEL};
`;

export const PriceStockCart = styled.div`
  margin-top: 10px;
  display: grid;
  grid-template-areas: 
  "price cart"
  "stock cart";
  grid-row-gap: 5px;
`;

