import React from 'react';
import {
  Container,
  Price,
  Cart,
  Name,
  StockInfo,
  Photo,
  PriceStockCart,
} from './product-card.style';
import {useHistory} from 'react-router';

interface IProductCardProps {
  name: string,
  manufacturer: string,
  image: string,
  stockStatus: boolean,
  price: string,
  category: string,
  type: string,
  id: string,
}

/**
 * Product card - component that responsible for the visualization of product in catalog or in product-slider
 * @returns JSX.Element
 */
export default function ProductCard(props: IProductCardProps): JSX.Element {
  const {name, manufacturer, image, stockStatus, price, category, id, type} = props;
  const history = useHistory();
  return (
    <Container onClick={() => {
      history.push(`/catalog/${category}/${type}/${id}`);
    }}>
      <Photo image={image}/>
      <Name>
        {`${manufacturer} ${name}`}
      </Name>
      <PriceStockCart>
        <Price>
          {`${price} грн`}
        </Price>
        <StockInfo stockStatus={stockStatus}>
          {stockStatus ? 'В наличии' : 'Нет в наличии'}
        </StockInfo>
        <Cart stockStatus={stockStatus}
          onClick={() => alert('Притворимся будто-бы добавили в корзину, но на деле ее не существует')}
        />
      </PriceStockCart>
    </Container>
  );
}
