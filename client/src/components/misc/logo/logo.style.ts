// @ts-ignore
import {ReactComponent as Diaphragm} from '../../../resources/diaphragm.svg';
import styled from 'styled-components';
import {PALETTE} from '../../../theme/theme';
import {ILogoProps} from './logo';
import {Link} from 'react-router-dom';

export const Container = styled(Link)`
  text-decoration: none;
`;

export const PrimaryText = styled.p<ILogoProps>`
  color: ${(props) => (props.isColored ? PALETTE.ALIZARIN_CRIMSON : PALETTE.NOBEL)};
  font-size: 42px;
  display: inline-block;
`;

export const SecondaryText = styled.p<ILogoProps>`
  color: ${(props) => (props.isColored ? PALETTE.APPLE : PALETTE.NOBEL)};
  font-size: 42px;
  font-weight: bold;
  display: inline-block;
`;

export const StyledDiaphragm = styled(Diaphragm)`
  fill: ${(props) => (props.isColored ? PALETTE.APPLE : PALETTE.NOBEL)};
  margin: 0 2px;
`;
