import React from 'react';
import {PrimaryText, SecondaryText, StyledDiaphragm, Container} from './logo.style';
import {URN} from '../../../config/constants';


export interface ILogoProps {
    isColored?: boolean
}

/**
 * Product logo.
 * @prop isColored - boolean value allowing an app to define whether to display colored or grayscale logo.
 * @remarks Logo has two display modes: colored (in brand colors) and grayscale.
 * @returns JSX.Element
 */
export default function Logo(props: ILogoProps): JSX.Element {
  const {isColored} = props;

  return (
    <Container to={URN.HOMEPAGE} >
      <PrimaryText isColored={isColored}>ABC</PrimaryText>
      <SecondaryText isColored={isColored}>f</SecondaryText>
      <StyledDiaphragm isColored={isColored}/>
      <SecondaryText isColored={isColored}>to</SecondaryText>
    </Container>
  );
}
