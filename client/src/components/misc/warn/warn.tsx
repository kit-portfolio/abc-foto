import React from 'react';
import {Container, Header, Message} from './warn.style';

/**
 * Warn is component to be used as stub
 * @remarks Does Marcellus Wallace look like a bitch!?
 * @returns JSX.Element
 */
export default function Warn(): JSX.Element {
  return (
    <Container>
      <Header>restricted area</Header>
      <Message>trespassers will be shot</Message>
      <Message>survivors will be shot again</Message>
    </Container>
  );
}
