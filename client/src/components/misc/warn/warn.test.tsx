import React from 'react';
import {cleanup, render, screen} from '@testing-library/react';
import Warn from './warn';

export enum TEST_ID {
    HEADER = 'warning-header',
    TEXT_1 = 'warning-text-1',
    TEXT_2 = 'warning-text-2'
}

describe.skip('Warn component', () => {
  beforeEach(() => {
    render(<Warn/>);
  });

  afterEach(() => cleanup());

  test('Header is ok', () => {
    const target = screen.getByTestId(TEST_ID.HEADER);
    expect(target).toBeInTheDocument();
  });

  test('Text 1 is ok', () => {
    const target = screen.getByTestId(TEST_ID.TEXT_1);
    expect(target).toBeInTheDocument();
  });

  test('Text 2 is ok', () => {
    const target = screen.getByTestId(TEST_ID.TEXT_2);
    expect(target).toBeInTheDocument();
  });
});
