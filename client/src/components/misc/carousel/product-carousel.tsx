import React, {useState, useEffect} from 'react';
import {
  SliderContainer,
  Wrapper,
  SliderTrack,
  SwipeButton,
  SliderContent,
} from './default/default-carousel.style';
import ClipLoader from 'react-spinners/ClipLoader';
import ProductCard from '../product-card/product-card';
import {
  BUTTON_TYPE,
  handleEventDirectionController,
  ICONFIGURATION,
} from './default/function';
import {PALETTE} from '../../../theme/theme';
import {fetchData} from '../../../config/fetch';
import {IProduct} from '../../../types/product';
import {env} from '../../../config/environment';
import {ENDPOINT} from '../../../config/constants';

const CONFIGURATION: ICONFIGURATION = {
  CONTENT_WIDTH: '260px',
  MARGIN: '20px',
  PADDING: '40px',
  SHIFT: '-5px',
  PADDING_FOR_SHADOW: '10px',
  STRIDE_LENGTH: 280,
  STEP: 1,
  ANIMATION_DURATION: '0.5s',
};

/**
 * ProductCarousel - slider displaying product cards.
 * @remarks Fetches items from api/products.
 * @returns JSX.Element
 */
export default function ProductCarousel(): JSX.Element {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [items, setItems] = useState<IProduct[]>([]);
  const [position, setPosition] = useState<number>(0);
  const quantity: number = items.length;
  const maxDisplacement: number = (quantity - 4) * CONFIGURATION.STRIDE_LENGTH;

  useEffect(() => {
    fetchData({
      href: `${env.apiBaseUrl}${ENDPOINT.PRODUCTS}`,
      setItems,
      setIsLoaded,
      shuffle: true,
    });
  }, []);

  const contentFormation = items.map((item: IProduct): JSX.Element => {
    return (
      <SliderContent
        width={CONFIGURATION.CONTENT_WIDTH}
        haveSelfHeight
        key={item._id}>
        <ProductCard
          image={item.images[0].image}
          stockStatus={item.quantity !== 0}
          price={item.price}
          manufacturer={item.manufacturer}
          name={item.name}
          type={item.type}
          category={item.category}
          id={item._id}
        />
      </SliderContent>
    );
  });

  if (!isLoaded) {
    return <ClipLoader/>;
  } else {
    return (
      <Wrapper padding={CONFIGURATION.PADDING}>
        <SwipeButton
          onClick={() => handleEventDirectionController({
            maxDisplacement,
            setPosition,
            type: BUTTON_TYPE.PREV,
            position,
            strideLength: CONFIGURATION.STRIDE_LENGTH,
            step: CONFIGURATION.STEP,
          })}
          left={CONFIGURATION.SHIFT}
          color={PALETTE.NOBEL}/>
        <SwipeButton
          onClick={() => handleEventDirectionController({
            maxDisplacement,
            setPosition,
            type: BUTTON_TYPE.NEXT,
            position,
            strideLength: CONFIGURATION.STRIDE_LENGTH,
            step: CONFIGURATION.STEP,
          })}
          right={CONFIGURATION.SHIFT}
          color={PALETTE.NOBEL}/>
        <SliderContainer isWrapped>
          <SliderTrack
            position={position}
            duration={CONFIGURATION.ANIMATION_DURATION}
            margin={CONFIGURATION.MARGIN}
            shadowPadding={CONFIGURATION.PADDING_FOR_SHADOW}>
            {contentFormation}
          </SliderTrack>
        </SliderContainer>
      </Wrapper>
    );
  }
}
