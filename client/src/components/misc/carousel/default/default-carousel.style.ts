import styled from 'styled-components';
import {PALETTE} from '../../../../theme/theme';
import {darken} from 'polished';
// @ts-ignore
import {ReactComponent as SwipeButtonIcon} from '../../../../resources/chevron-simple.svg';

interface IWrapperProps {
  padding: string | undefined,
}

export const Wrapper = styled.div<IWrapperProps>`
    box-sizing: border-box;
    width: 1200px;
    padding: ${(props) => `0 ${props.padding}`};
    position: relative;        
`;

interface ISliderContainerProps {
  isWrapped?: boolean
}

export const SliderContainer = styled.div<ISliderContainerProps>`
  height: 380px;
  width: ${(props) => props.isWrapped ? null : '1200px'}; 
  margin: 0 auto;
  overflow: hidden;
  position: relative;
`;

interface ISliderContentProps {
  width: string,
  image?: string,
  haveSelfHeight: boolean,
}

export const SliderContent = styled.div<ISliderContentProps>`
  cursor: pointer;
  min-width: ${(props) => props.width};
  background-image: url(${(props) => props.image ? props.image : 'none'});
  height: ${(props) => props.haveSelfHeight ? null : '100%'};
`;

interface ISliderTrackProps {
  position: number,
  margin?: string,
  shadowPadding?: string,
  duration: string
}

export const SliderTrack = styled.div<ISliderTrackProps>`
  transition: ${(props) => props.duration} ease;
  transform: translateX(-${(props) => props.position}px);
  height: 100%;
  display: flex;
  align-items: center;
  padding: ${(props) => props.shadowPadding ? `0 ${props.shadowPadding}` : null};
  
  & ${SliderContent} {
    margin-right: ${(props) => props.margin ? props.margin : 0} ;
  }
  
  & ${SliderContent}:last-child {
    margin-right: 0;
  }
`;

interface ISwipeButton {
  color: string,
  left?: string,
  right?: string,
}

export const SwipeButton = styled(SwipeButtonIcon)<ISwipeButton>`
 cursor: pointer;
 height: 40px;
 width: 40px;
 position: absolute;
 z-index: 1;
 top: 44%;
 fill: ${PALETTE.WHITE};
 left: ${(props) => props.left ? props.left : null};
 right: ${(props) => props.right ? props.right : null};
 background: ${(props) => props.color};
 transform: ${(props) => props.right ? 'rotate(270deg)' : 'rotate(90deg)'};
 
  &:hover {
    background: ${(props) => darken(0.1, props.color)};
    transition: 0.3s;
  }
`;

interface IDotsLineProps {
  quantity: number
}

export const DotsLine = styled.div<IDotsLineProps>`
  position: absolute;
  z-index: 1;
  top: 90%;
  left: calc((1200px - 15*${(props) => props.quantity}px)/2);
  display: grid;
  grid-column-gap: 6px;
  grid-template-columns: repeat(${(props) => props.quantity}, 1fr);
`;

interface IDotsProps {
  isActive?: boolean
}

export const Dots = styled.div<IDotsProps>`
  width: 15px;
  height: 15px;
  background:  ${(props) => props.isActive ? PALETTE.APPLE : PALETTE.WHITE};
  cursor: pointer;
`;

