import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {StyledLink} from './page-404.style';
import ErrorPage from '../error-page/error-page';
// @ts-ignore
import {ReactComponent as Icon404} from '../../../resources/404.svg';
import {BREADCRUMBS_ACTION} from '../../../store/breadcrumbs/breadcrumbs.actions';

/**
 * page-404 - the page that user sees when he goes to a non-existent page
 * @returns JSX.Element
 */
export default function Page404(): JSX.Element {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({type: BREADCRUMBS_ACTION.CLEAR});
  }, []);

  return (
    <ErrorPage message={
      <span>
         Take me back to <StyledLink to={'/'}>Home</StyledLink>
      </span>
    } image={<Icon404/>} header='Page not found'/>
  );
}
