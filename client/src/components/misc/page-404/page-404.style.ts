import styled from 'styled-components';
import {PALETTE} from '../../../theme/theme';
import {Link} from 'react-router-dom';


export const StyledLink = styled(Link)`
  text-decoration: none;
  border-bottom: 2px solid ${PALETTE.PALE_SKY};
  color: ${PALETTE.PALE_SKY};
`;
