import styled from 'styled-components';
import {PALETTE, FONT_SIZE} from '../../../theme/theme';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 1200px;
  align-items: center;
  justify-self: center;
  & svg {
    height: 256px;
    width: 256px;
    fill: ${PALETTE.NOBEL};
  }
`;

export const Header = styled.h2`
  ${FONT_SIZE.HEADER};
  text-transform: uppercase;
  font-weight: bold;
  color: ${PALETTE.APPLE};
`;

export const Message = styled.p`
  font-weight: bold;
  ${FONT_SIZE.TEXT};
  color: ${PALETTE.PALE_SKY};
`;
