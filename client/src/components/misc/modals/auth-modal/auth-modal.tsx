import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {
  DarkenedBackground,
  LoginWindowBox,
  HeaderWrapper,
  GreyLine,
  TwitterIcon,
  FacebookIcon,
  GoogleIcon,
  CloseButton,
  TabsWrapper,
  IconWrapper,
} from './auth-modal.style';
import LogIn from './forms/login-form';
import SignUp from './forms/signup-form';
import {TODO_ANY} from '../../../../types/misc';
import {closeModal} from '../../../../store/modal-controller/modal-controller.actions';

enum TABS {
  LOG_IN = 'Войти',
  SIGN_IN = 'Зарегистрироваться'
}

/**
 * This component appears when user try to signup or logIn.
 * @returns JSX.Element
 */
export default function AuthModal(): JSX.Element {
  const [activeTab, setActiveTab] = useState(TABS.LOG_IN);
  const dispatch = useDispatch();
  const md5 = require('md5');

  /**
   * This function renders an array of AuthModal tabs
   * @returns Array of JSX.Element
   */
  function renderTabs(): JSX.Element[] {
    return [TABS.LOG_IN, TABS.SIGN_IN].map((item) => {
      return <TabsWrapper
        key={md5(item)}
        onClick={() => setActiveTab(item)}
        isActive={item === activeTab}
      >{item}
      </TabsWrapper>;
    });
  }

  /**
   * This function renders profile tab contents
   * @returns JSX.Element
   */
  function renderContent(target: TABS): TODO_ANY {
    switch (target) {
      case TABS.LOG_IN:
        return <LogIn/>;
      case TABS.SIGN_IN:
        return <SignUp/>;
    }
  }

  return (
    <DarkenedBackground>
      <LoginWindowBox>
        <CloseButton onClick={() => dispatch(closeModal())}/>
        <HeaderWrapper>
          {renderTabs()}
        </HeaderWrapper>
        {renderContent(activeTab)}
        <GreyLine/>
        <IconWrapper>
          <FacebookIcon/>
          <TwitterIcon/>
          <GoogleIcon/>
        </IconWrapper>
      </LoginWindowBox>
    </DarkenedBackground>
  );
}
