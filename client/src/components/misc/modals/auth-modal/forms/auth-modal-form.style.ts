import styled from 'styled-components';
import {PALETTE} from '../../../../../theme/theme';
import {LabelText} from '../../../../../theme/form';
import {Link} from 'react-router-dom';

export const Form = styled.form`
  width: 100%;
  margin-top: 25px;
`;

export const Checkbox = styled.input`
  width: 16px;
  height: 16px;
  border: 1px solid ${PALETTE.LIGHT_GRAY};
  margin-right: 9px;
  position: relative;
  top: 2px;
`;

export const AdditionalFormInfo = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 20px;
`;

export const TextNearCheckbox= styled(LabelText)`
  margin-bottom: 0;
`;

export const ForgotPass = styled(Link)`
  ${LabelText};
  text-decoration: none;
`;
