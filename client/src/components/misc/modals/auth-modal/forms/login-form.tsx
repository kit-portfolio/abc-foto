import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import {useFormik} from 'formik';
import {toast} from 'material-react-toastify';
import {
  LabelText,
  Label,
  TextInput,
  ValidationMessage,
  PasswordInput,
  ActionButton,
} from '../../../../../theme/form';
import {
  Form,
  Checkbox,
  TextNearCheckbox,
  AdditionalFormInfo,
  ForgotPass,
} from './auth-modal-form.style';
import * as Yup from 'yup';
import {
  PLACEHOLDER,
  emailValidationRules,
  passwordValidationRules,
} from '../../../../../config/form';
import {closeModal} from '../../../../../store/modal-controller/modal-controller.actions';
// @ts-ignore
import {ReactComponent as EyeIcon} from '../../../../../resources/eye.svg';
import axios from 'axios';
import {logIn} from '../../../../../store/auth/auth.actions';
import {TODO_ANY} from '../../../../../types/misc';
import {env} from '../../../../../config/environment';
import {ENDPOINT} from '../../../../../config/constants';

const LoginFormValidationSchema = Yup.object().shape({
  email: emailValidationRules,
  password: passwordValidationRules,
});

/**
 * handleEventForgotPassword describes the action on clicking on jsx.elem(ForgotPass)
 * @returns void
 */
function handleEventForgotPassword(): void {
  alert('Stub\nВы забыли пароль');
}

/**
 * LoginForm appears at authentication modal and intended to collect existing user login data.
 * @returns JSX.Element
 */
export default function LoginForm(): JSX.Element {
  const dispatch = useDispatch();
  const [isProtected, setIsProtected] = useState(true);
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      anotherComputer: false,
    },
    validationSchema: LoginFormValidationSchema,
    onSubmit: (values, {resetForm, setFieldError}) => {
      axios.post(`${env.apiBaseUrl}${ENDPOINT.LOGIN}`,
          {
            email: values.email,
            password: values.password,
          })
          .then((res) => {
            logIn(dispatch, res.data.token);
            resetForm();
            dispatch(closeModal());
          })
          .catch((e: TODO_ANY) => {
            const error = !!e.response ? e.response.data : {field: null};
            if (error.field === 'Password') {
              setFieldError('password', error.message);
            } else if (error.field === 'Email') {
              setFieldError('email', error.message);
            } else {
              toast.error('Something went wrong on server');
            }
          });
    },
  });
  const error = formik.errors;
  const touched = formik.touched;
  return (
    <Form onSubmit={formik.handleSubmit}>
      <Label>
        <LabelText>
          E-mail
        </LabelText>
        {error.email && touched.email ?
          (<ValidationMessage>{error.email}</ValidationMessage>) :
          null}
        <TextInput
          name='email'
          type='text'
          value={formik.values.email}
          onChange={formik.handleChange}
          placeholder={PLACEHOLDER.EMAIL}/>
      </Label>
      <Label>
        <LabelText>
          Пароль
        </LabelText>
        {error.password && touched.password ?
          (<ValidationMessage>{error.password}</ValidationMessage>) :
          null}
        <PasswordInput hidePassword={isProtected}>
          <input
            name='password'
            type={isProtected ? 'password' : 'text'}
            value={formik.values.password}
            onChange={formik.handleChange}
            placeholder={PLACEHOLDER.PASSWORD}/>
          <EyeIcon
            onMouseDown={() => setIsProtected(false)}
            onMouseUp={() => setIsProtected(true)}/>
        </PasswordInput>
      </Label>
      <AdditionalFormInfo>
        <Label>
          <Checkbox
            name='anotherComputer'
            type='checkbox'
            onChange={formik.handleChange}
          />
          <TextNearCheckbox>
            Чужой компьютер
          </TextNearCheckbox>
        </Label>
        <ForgotPass to='/' onClick={handleEventForgotPassword}>Забыли пароль?</ForgotPass>
      </AdditionalFormInfo>
      <ActionButton onClick={() => formik.submitForm()}>
        Войти
      </ActionButton>
    </Form>
  );
}
