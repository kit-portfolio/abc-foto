import styled from 'styled-components';
import {FONT_SIZE, PALETTE} from '../../../../theme/theme';
// @ts-ignore
import {ReactComponent as Twitter} from '../../../../resources/modal-window-resources/twitter.svg';
// @ts-ignore
import {ReactComponent as Facebook} from '../../../../resources/modal-window-resources/facebook.svg';
// @ts-ignore
import {ReactComponent as Google} from '../../../../resources/modal-window-resources/google.svg';
// @ts-ignore
import {ReactComponent as Cross} from '../../../../resources/close.svg';
import {transparentize} from 'polished';

export const DarkenedBackground = styled.div`
  position: fixed;
  z-index: 3;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  padding-top: 120px;
  width: 100%;
  height: 100%;
  background: ${transparentize(0.35, PALETTE.BLACK)};
  text-align: center;
`;

export const LoginWindowBox = styled.div`
  position: relative;
  box-sizing: border-box;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  width: 468px;
  padding: 40px;
  height: min-content;
  background: ${PALETTE.LIGHT_GRAY};
`;

export const HeaderWrapper = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 2.3fr;
  
`;

interface ITabsWrapperProps{
  isActive: boolean
}
export const TabsWrapper = styled.div<ITabsWrapperProps>`
  border-bottom: ${(props) => props.isActive ? '9px solid' : '1px solid'};
  border-color: ${PALETTE.APPLE};
  ${FONT_SIZE.SUB_HEADER};
  font-weight: 700;
  padding-bottom: 2px;
  color: ${(props) => props.isActive ? PALETTE.APPLE : PALETTE.BLACK};
`;

export const GreyLine = styled.div`
  height: 1px;
  width: 100%;
  background: ${PALETTE.NOBEL};
  margin-bottom: 30px;
  margin-top: 46px;
`;

export const IconWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-column-gap: 25px;
`;

const iconSize = `
  height: 54px;
  width: 54px;  
`;
export const TwitterIcon = styled(Twitter)`
  ${iconSize};
`;

export const FacebookIcon = styled(Facebook)`
  ${iconSize};
`;

export const GoogleIcon = styled(Google)`
  ${iconSize};
`;

export const CloseButton = styled(Cross)`
  position: absolute;
  top: -30px;
  right: -30px;
  cursor: pointer;
`;
