import styled from 'styled-components';
import {CSSProperties} from 'react';
import {PALETTE, FONT_SIZE} from '../../../../theme/theme';
import {transparentize} from 'polished';
// @ts-ignore
import {ReactComponent as Cross} from '../../../../resources/close.svg';

export const OverlayStyle: CSSProperties = {
  backgroundColor: transparentize(0.6, `${PALETTE.BLACK}`),
};

export const ContentStyle: CSSProperties = {
  width: '380px',
  top: '50%',
  left: '50%',
  right: 'auto',
  bottom: 'auto',
  transform: 'translate(-50%, -50%)',
  overflow: 'visible',
  textAlign: 'center',
  padding: '40px 60px',
};

export const Title = styled.h2`
  color: ${PALETTE.APPLE};
  ${FONT_SIZE.HEADER};
`;

export const Message = styled.p`
  ${FONT_SIZE.TEXT};
  line-height: 22px;
`;

export const ActionsBox = styled.div`
  display: flex;
  gap: 15px;
  margin-top: 35px;
`;

export const CloseButton = styled(Cross)`
  position: absolute;
  right: -40px;
  top: -22px;
`;


