import styled from 'styled-components';
import {FONT_SIZE, PALETTE} from '../../../theme/theme';
import {ContentBox} from '../../../theme/common';
import {darken} from 'polished';

interface IDefaultTextProps {
  isActive?: boolean;
}

export const DefaultText = styled.span<IDefaultTextProps>`
  ${FONT_SIZE.SMALL_TEXT};
  color: ${(props) => props.isActive ? PALETTE.NOBEL : PALETTE.BLACK};
`;

export const ProductTitle = styled.span`
  ${FONT_SIZE.HEADER};
  font-weight: 700;
  margin-right: 100px;
  text-transform: uppercase;
`;

export const ItemPageContainer = styled(ContentBox)`
  display: flex;
  flex-direction: column;
`;

export const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-top: 40px;
`;

export const TabsContainer = styled.div`
  width: 100%;
  height: 75px;
  display: grid;
  margin-top: 30px;
  grid-template-columns: repeat(3, 1fr);
`;

interface ITabProps {
  isActive: boolean,
}

export const Tab = styled.div<ITabProps>`
  background: ${(props) => props.isActive ? darken(0.05, PALETTE.LIGHT_GRAY) : PALETTE.LIGHT_GRAY};
  display: flex;
  align-items: center;
  cursor: pointer;
`;

interface ITabValueProps {
  isCentral: boolean,
  activeTab: string
}

export const TabValue = styled.div<ITabValueProps>`
  ${FONT_SIZE.TEXT};
  height: 43px;
  font-weight: 600;
  width: 100%;
  text-align: center;
  line-height: 43px;
  border-right: ${(props) => (props.activeTab !== 'АКСЕССУАРЫ' && props.isCentral) ? '1px solid' : 'none'};
  border-left: ${(props) => (props.activeTab !== 'ОПИСАНИЕ' && props.isCentral) ? '1px solid' : 'none'};
  border-color: ${darken(0.05, PALETTE.LIGHT_GRAY)};
`;
