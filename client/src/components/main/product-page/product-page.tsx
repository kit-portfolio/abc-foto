import React, {useEffect, useState} from 'react';
import {
  DefaultText,
  ProductTitle,
  ItemPageContainer,
  TitleWrapper,
  TabsContainer,
  TabValue,
  Tab,
} from './product-page.style';
import MainInfo from './main-info/main-info';
import ErrorBoundary from '../../misc/error-boundary/error-boundary';
import ClipLoader from 'react-spinners/ClipLoader';
import {TODO_ANY} from '../../../types/misc';
import axios from 'axios';
import {IProduct, emptyProduct} from '../../../types/product';
import {env} from '../../../config/environment';
import {ENDPOINT} from '../../../config/constants';

enum TABS {
  DESCRIPTION = 'ОПИСАНИЕ',
  CHARACTERISTIC = 'ХАРАКТЕРИСТИКИ',
  ACCESSORIES = 'АКСЕССУАРЫ'
}

interface IProductPageProps {
  id: string,
}

/**
 * Item page that takes all the information about a product from backEnd
 * @remarks This component brings together Header(ProductCategory, TitleWrapper), MainInfo and AdditionalInfo
 * @returns JSX.Element
 */
export default function ProductPage(props:IProductPageProps): JSX.Element {
  const md5 = require('md5');
  const [activeTab, setActiveTab] = useState<TABS>(TABS.DESCRIPTION);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [data, setData] = useState<IProduct>(emptyProduct);
  useEffect(() => {
    axios.get(`${env.apiBaseUrl}${ENDPOINT.PRODUCTS}/${props.id}`)
        .then((item: TODO_ANY) => {
          setData(item.data);
          setIsLoaded(true);
        })
        .catch((e) => console.log(e.response));
  }, [props.id]);

  /**
   * This function renders an array of MainInfo tabs
   * @returns Array of JSX.Element
   */
  function renderTabs(): JSX.Element[] {
    return [TABS.DESCRIPTION, TABS.CHARACTERISTIC, TABS.ACCESSORIES].map((item, index) => {
      return <Tab
        key={md5(item)}
        onClick={() => setActiveTab(item)}
        isActive={item === activeTab}>
        <TabValue activeTab={activeTab} isCentral={index === 1}>
          {item}
        </TabValue>
      </Tab>;
    });
  }
  if (!isLoaded) {
    return <ClipLoader/>;
  }

  return (
    <ErrorBoundary>
      <ItemPageContainer>
        <TitleWrapper>
          <ProductTitle>
            {`${data.manufacturer} ${data.name}`}
          </ProductTitle>
          <DefaultText>
              Код товара : {data.code}
          </DefaultText>
        </TitleWrapper>
        <MainInfo images={data.images} stockStatus={data.quantity !== 0} price={+data.price} id={props.id}/>
        <TabsContainer>
          {renderTabs()}
        </TabsContainer>
      </ItemPageContainer>
    </ErrorBoundary>
  );
}
