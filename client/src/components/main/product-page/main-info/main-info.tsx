import React, {useState} from 'react';
import {
  SmallImageWrapper,
  Container,
  MainContentImage,
  MainInfoContainer,
  Cart,
  Button,
  ContentWrapper,
  InStockInfo,
  Price,
  StaticListInfo,
  StaticInfo,
  StaticListInfoHeader,
  StaticListInfoLink,
  AdditionalInfo,
} from './main-info.style';
import Slider from './slider/slider';
import {IImage} from '../../../../types/product';
import {useSelector} from 'react-redux';
import {TODO_ANY} from '../../../../types/misc';
import axios from 'axios';
import {toast} from 'material-react-toastify';

interface IMainInfoProps {
  price: number,
  stockStatus: boolean,
  images: IImage[],
  id: string,
}

const defaultUser = {
  userId: '',
  comment: 'I have two number nine, number nine large, number six with extra ' +
    'dip,number seven, two number fourty-fifth, one with cheese and a large soda',
  client: {
    firstName: 'Валентина',
    lastName: 'Васильчук',
    phone: '0933369234',
    email: 'mock@example.org',
  },
  products: ['dummy-product-id', 'dummy-product-id', 'dummy-product-id', 'dummy-product-id'],
  shipping: {
    type: 'Pickup',
    address: 'м. Івано-Франківськ, вул. Олександра Довженка 29',
    price: 30,
  },
  total: 11200,
  status: 'Pending',
};

/**
 * This component renders basic information about item (photo, price, button "Buy").
 * @prop price - Product price
 * @prop inStock - Boolean value showing if product is in stock.
 * @prop images - An array of images URL's.
 * @returns JSX.Element
 */
export default function MainInfo(props: IMainInfoProps): JSX.Element {
  const {price, stockStatus, images} = props;
  const userId = useSelector((state:TODO_ANY) => state.auth.id);
  const userData = useSelector((state:TODO_ANY) => state.user);
  const [activeMainImage, setActiveMainImage] = useState<number>(0);
  const md5 = require('md5');
  const staticInfoArray: string[] = ['Доставка по всей Украине', 'Оплата товара при получении', 'Возможен самовывоз'];
  const staticInfoJsx = staticInfoArray.map((item) => {
    return (
      <StaticListInfo key={md5(item)}>{item}</StaticListInfo>
    );
  });

  /**
   * handleEvent describes the action on clicking on jsx.elem(all button)
   * @returns void
   */
  function handleEvent(): void {
    alert('Stub\nВы нажали на кнопку');
  }

  /**
   * purchase - event that creates a new buy order
   * @returns void
   */
  function purchase() {
    axios.post('http://localhost:5000/api/orders', {
      userId: userId || defaultUser.userId,
      comment: '-',
      client: {
        firstName: userData.firstName || defaultUser.client.firstName,
        lastName: userData.lastName || '',
        phone: userData.phone || defaultUser.client.phone,
        email: userData.email || defaultUser.client.email,
      },
      products: [props.id],
      shipping: {
        type: defaultUser.shipping.type,
        address: defaultUser.shipping.address,
        price: defaultUser.shipping.price,
      },
      total: price,
      status: 'Pending',
    })
        .then((res) => {
          alert('Покупка удалась. Загляните в консоль');
          if (userId) {
            console.log('Покупка удалась. Визуальное изменение можно увидеть в личном кабинете => заказы');
          } else {
            console.log('Покупка удалась. Поскольку вы не зарегистрированы' +
              '(а спрашивать данные долго) заказ оформлен на другого юзера. Ответ от сервера ниже');
            console.log(res.data);
          }
          toast.success('Покупка удалась');
        })
        .catch(() => toast.error('Something went wrong on server'));
  }

  return (
    <Container>
      <SmallImageWrapper>
        <Slider images={images} setActiveMainImage={setActiveMainImage}/>
      </SmallImageWrapper>
      <MainContentImage background={images[activeMainImage].image}/>
      <MainInfoContainer>
        <InStockInfo stockStatus={stockStatus}>{stockStatus ? 'в наличии' : 'отсутвует'}</InStockInfo>
        <Price>{price} грн</Price>
        <ContentWrapper>
          <Button onClick={() => purchase()}>
            Купить
            <Cart/>
          </Button>
          <Button onClick={() => handleEvent()} isCredit={true}>
            Купить в кредит
          </Button>
        </ContentWrapper>
        <ContentWrapper>
          <AdditionalInfo>
            <StaticListInfoHeader>
              ДОСТАВКА
            </StaticListInfoHeader>
            {staticInfoJsx}
          </AdditionalInfo>
          <AdditionalInfo>
            <StaticListInfoHeader>
              ГАРАНТИЯ
            </StaticListInfoHeader>
            <StaticInfo>
              Официальная 24 месяца от производителя
            </StaticInfo>
            <StaticListInfoLink to={'/'}>
              Условия доставки и оплаты
            </StaticListInfoLink>
          </AdditionalInfo>
        </ContentWrapper>
      </MainInfoContainer>
    </Container>
  );
}
