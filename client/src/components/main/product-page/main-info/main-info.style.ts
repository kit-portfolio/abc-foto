import styled from 'styled-components';
import {FONT_SIZE, PALETTE} from '../../../../theme/theme';
// @ts-ignore
import {ReactComponent as SwipeButtonIcon} from '../../../../resources/chevron-simple.svg';
// @ts-ignore
import {ReactComponent as CartIcon} from '../../../../resources/cart.svg';
import {Link} from 'react-router-dom';
import {ActionButton} from '../../../../theme/form';
import {darken} from 'polished';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 30px;
  text-align: left;
`;

export const SmallImageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100px;
  height: 420px;
  justify-content: space-between;
`;

interface ISmallImagePage {
  background: string,
}

export const SmallImage = styled.div<ISmallImagePage>`
  box-sizing: border-box;
  cursor: pointer;
  height: 100px;
  width: 100%;
  background-image: url(${(props) => props.background});
  background-repeat: no-repeat;
  background-size: 85%;
  background-position: center;
  border: 1px solid black;
`;

export const SwipeButtonWrapper = styled.div`
  display: grid;
  cursor: pointer;
  margin-top: 2px;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 2px;
`;

interface ISwipeButton {
  isRotating?: boolean,
}

export const SwipeButton = styled(SwipeButtonIcon)<ISwipeButton>`
 background: ${PALETTE.APPLE};
 transform: ${(props) => props.isRotating ? 'rotate(180deg)' : '0'};
  &:hover {
    background: ${darken(0.1, PALETTE.APPLE)};
    transition: 0.3s;
  }
`;

interface IMainContentImageProps {
  background: string,
}

export const MainContentImage = styled.div<IMainContentImageProps>`
  width: 500px;
  height: 420px;
  background-image: url(${(props) => props.background});
  background-repeat: no-repeat;
  background-size: 85%;
  background-position: center;
`;

export const MainInfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 440px;
`;

interface IInStockInfoProps {
  stockStatus: boolean
}

export const InStockInfo = styled.span<IInStockInfoProps>`
  color: ${(props) => props.stockStatus ? PALETTE.APPLE : PALETTE.DARK_RED};
`;

export const Price = styled.span`
  ${FONT_SIZE.HEADER};
  font-weight: 700;
`;

export const ContentWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 40px;
`;

interface IButtonProps {
  isCredit?: boolean,
}

export const Button = styled(ActionButton)<IButtonProps>`
  ${FONT_SIZE.TEXT};
  box-sizing: border-box;
  width: 180px;
  display: flex;
  align-items: center;
  margin: 0;
  justify-content: ${(props) => props.isCredit ? 'center' : 'space-around'};
  background: ${(props) => props.isCredit ? PALETTE.PALE_SKY : PALETTE.APPLE};
  &:hover {
    background-color: ${(props) => props.isCredit ? darken(0.05, PALETTE.PALE_SKY) : darken(0.05, PALETTE.APPLE)};
  }
`;

export const Cart = styled(CartIcon)`
  height: 35px; width: 35px;
`;

export const StaticListInfoHeader = styled.span`
  ${FONT_SIZE.TEXT};
  font-weight: 600;
`;

export const StaticListInfo = styled.span`
  ${FONT_SIZE.SMALL_TEXT};
  line-height: 25px;
  &:before{
    content: '•';
    margin-right: 5px;
  }
`;

export const StaticInfo = styled.span`
  ${FONT_SIZE.SMALL_TEXT};
  line-height: 20px;
  max-width: 180px;
`;

export const StaticListInfoLink = styled(Link)`
  ${FONT_SIZE.SMALL_TEXT};
  color: ${PALETTE.SKY_BLUE};
  border-bottom: 1px dashed ${PALETTE.SKY_BLUE};
  text-decoration: none;
  margin-top: 11px;
  width: max-content;
`;

export const AdditionalInfo = styled.div`
  display: flex;
  flex-direction: column;
`;
