import React, {Dispatch, SetStateAction, useState} from 'react';
import {
  SliderContainer,
  SliderTrack,
} from './slider.style';
import {
  SmallImage,
  SwipeButton,
  SwipeButtonWrapper,
} from '../main-info.style';
import {IImage} from '../../../../../types/product';

enum BUTTON_TYPE {
  NEXT = 'Next',
  PREV = 'Prev'
}

interface ISliderProps {
  images: IImage[],
  setActiveMainImage: Dispatch<SetStateAction<number>>
}

/**
 * This component is a slider in main-info(product-page). Slider-items is a small-photo
 * @prop images - An array of images URL's.
 * @returns JSX.Element
 */
export default function Slider(props: ISliderProps): JSX.Element {
  const [position, setPosition] = useState<number>(0);
  const maxDisplacement: number = (props.images.length - 3) * 100;

  /**
   * This function handles click on SwipeButton and changes state depending on the type of the pressed button
   * @returns void
   */
  function handleEventDirectionController(type: string): void {
    if ((type === BUTTON_TYPE.NEXT) && (position < maxDisplacement)) {
      setPosition(position + 125);
    } else if ((type === BUTTON_TYPE.PREV) && (position > 0)) {
      setPosition(position - 125);
    } else return;
  }

  const ArrayOfImages = props.images.map((item, index): JSX.Element => {
    return (
      <SmallImage onClick={() => props.setActiveMainImage(index)} key={item._id} background={item.preview}/>
    );
  });

  return (
    <>
      <SliderContainer>
        <SliderTrack position={position}>
          {ArrayOfImages}
        </SliderTrack>
      </SliderContainer>
      <SwipeButtonWrapper>
        <SwipeButton onClick={() => handleEventDirectionController(BUTTON_TYPE.NEXT)}/>
        <SwipeButton onClick={() => handleEventDirectionController(BUTTON_TYPE.PREV)} isRotating={true}/>
      </SwipeButtonWrapper>
    </>
  );
}
