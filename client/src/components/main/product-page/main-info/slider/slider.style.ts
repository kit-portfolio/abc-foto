import styled from 'styled-components';

export const SliderContainer = styled.div`
  margin: 0 auto;
  height: 350px;
  width: 100px;
  overflow: hidden;
`;

interface ISliderTrackProps {
  position: number
}

export const SliderTrack = styled.div<ISliderTrackProps>`
  display: grid;
  grid-row-gap: 25px;
  transition: 0.3s;
  transform: translateY(-${(props) => props.position}px);
`;

