import React, {useEffect, useState} from 'react';
import {useHistory, withRouter} from 'react-router';
import {useDispatch} from 'react-redux';
import {logOut} from '../../../store/auth/auth.actions';
import {openInfoModal, closeModal} from '../../../store/modal-controller/modal-controller.actions';
import {ContentBox, toastConfig} from '../../../theme/common';
import {Header, Grid, Tab, LogoutIcon} from './profile.style';
import UserInfo from './user-info/user-info';
import UserPassword from './user-password/user-password';
import UserOrders from './user-orders/user-orders';
import {ActionButton} from '../../../theme/form';
import {PALETTE} from '../../../theme/theme';
import {toast} from 'material-react-toastify';
import ErrorBoundary from '../../misc/error-boundary/error-boundary';
import {URN} from '../../../config/constants';
import {BREADCRUMBS_ACTION} from '../../../store/breadcrumbs/breadcrumbs.actions';

enum TABS {
  INFO = 'Редактировать профиль',
  PASSWORD = 'Изменить пароль',
  ORDERS = 'Мои заказы',
}

/**
 * User profile page
 * @returns JSX.Element
 */
function Profile(): JSX.Element {
  const history = useHistory();
  const dispatch = useDispatch();
  const md5= require('md5');
  const [activeTab, setActiveTab] = useState(TABS.INFO);

  useEffect(() => {
    dispatch({type: BREADCRUMBS_ACTION.PROFILE});
  });

  /**
   * This function renders an array of profile tabs
   * @returns Array of JSX.Element
   */
  function renderTabs(): JSX.Element[] {
    return [TABS.INFO, TABS.PASSWORD, TABS.ORDERS].map((item) => {
      return <Tab
        key={md5(item)}
        onClick={() => setActiveTab(item)}
        isActive={item === activeTab}
      >{item}
      </Tab>;
    });
  }

  /**
   * This function renders profile tab contents
   * @returns JSX.Element
   */
  function renderContent(target: TABS): JSX.Element {
    switch (target) {
      case TABS.INFO: return <UserInfo/>;
      case TABS.PASSWORD: return <UserPassword/>;
      case TABS.ORDERS: return <UserOrders/>;
    }
  }

  /**
   * Log out event handler
   * @returns void
   */
  function handleLogOutClick(): void {
    dispatch(openInfoModal({
      image: <LogoutIcon/>,
      title: 'Confirm log out?',
      message: 'On confirm, your session will be finished and you will be redirected to the homepage',
      actions: [
        <ActionButton
          key={'cancel logout'}
          color={PALETTE.NOBEL}
          onClick={() => dispatch(closeModal())}
        >Cancel</ActionButton>,
        <ActionButton
          key={'confirm logout'}
          color={PALETTE.ALIZARIN_CRIMSON}
          onClick={() => {
            logOut(dispatch);
            dispatch(closeModal());
            history.push(URN.HOMEPAGE);
            toast.dark('Logged out', toastConfig);
          }}
        >Confirm</ActionButton>],
    }));
  }

  return (
    <ErrorBoundary>
      <ContentBox>
        <Header>личный кабинет</Header>
        <Grid>
          <div>
            {renderTabs()}
            <Tab onClick={() => handleLogOutClick()}>Выйти из кабинета</Tab>
          </div>
          {renderContent(activeTab)}
        </Grid>
      </ContentBox>
    </ErrorBoundary>
  );
}

export default withRouter(Profile);
