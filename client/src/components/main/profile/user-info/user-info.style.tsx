import styled from 'styled-components';

export const Grid = styled.form`
  display: grid;
  grid-column-gap: 80px;
  grid-row-gap: 20px;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 75px 75px 75px;
`;
