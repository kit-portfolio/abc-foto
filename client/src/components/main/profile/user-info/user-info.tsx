// Modules
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import 'yup-phone';
import {TODO_ANY} from '../../../../types/misc';
import axios from 'axios';
// Styling
import {Grid} from './user-info.style';
import {
  Label,
  LabelText,
  TextInput,
  ValidationMessage,
  ActionButton,
  Dropdown,
} from '../../../../theme/form';
import {
  PLACEHOLDER,
  nameValidationRules,
  surnameValidationRules,
  phoneValidationRules,
  emailValidationRules,
  genderValidationRules,
  birthdayValidationRules,
} from '../../../../config/form';
import {toast} from 'material-react-toastify';
import {toastConfig} from '../../../../theme/common';
import {createWriteUserAction} from '../../../../store/user/user.actions';
import {env} from '../../../../config/environment';
import {ENDPOINT} from '../../../../config/constants';

const UserInfoSchema = Yup.object().shape({
  firstName: nameValidationRules,
  lastName: surnameValidationRules,
  phone: phoneValidationRules,
  email: emailValidationRules,
  gender: genderValidationRules,
  birthDate: birthdayValidationRules,
});

/**
 * User profile page
 * @returns JSX.Element
 */
export default function UserInfo(): JSX.Element {
  const dispatch = useDispatch();
  const auth = useSelector((state:TODO_ANY) => state.auth);
  const user = useSelector((state:TODO_ANY) => state.user);
  const formik = useFormik({
    initialValues: {
      firstName: user.firstName ? user.firstName : '',
      lastName: user.lastName ? user.lastName : '',
      email: user.email ? user.email : '',
      phone: user.phone ? user.phone : '',
      gender: user.gender ? user.gender : '',
      birthDate: user.birthDate ? user.birthDate : '',
    },
    validationSchema: UserInfoSchema,
    onSubmit: (values, {setFieldError}) => {
      axios.put(
          `${env.apiBaseUrl}${ENDPOINT.USERS}`,
          values,
          {
            headers: {
              'Authorization': auth.token,
            }})
          .then((res) => {
            dispatch(createWriteUserAction(res.data));
            toast.success('User updated', toastConfig);
          })
          .catch((e : TODO_ANY) => {
            if (!!e.response && e.response.data.field === 'Email') {
              setFieldError('email', e.response.data.message);
            }
            toast.error('User update failed', toastConfig);
          });
    },
  });

  return (
    <Grid action="#" onSubmit={formik.handleSubmit}>
      <Label htmlFor="firstName">
        <LabelText>Имя</LabelText>
        {formik.errors.firstName && formik.touched.firstName ?
          (<ValidationMessage>{formik.errors.firstName}</ValidationMessage>) :
          null}
        <TextInput
          id='firstName'
          type='text'
          name='firstName'
          value={formik.values.firstName}
          onChange={formik.handleChange}
          placeholder={PLACEHOLDER.NAME}
        />
      </Label>
      <Label htmlFor="phone">
        <LabelText>Телефон</LabelText>
        {formik.errors.phone && formik.touched.phone ?
          (<ValidationMessage>{formik.errors.phone}</ValidationMessage>) :
          null}
        <TextInput
          id='phone'
          type='tel'
          name='phone'
          value={formik.values.phone}
          onChange={formik.handleChange}
          placeholder={PLACEHOLDER.PHONE}
        />
      </Label>
      <Label htmlFor="lastName">
        <LabelText>Фамилия</LabelText>
        {formik.errors.lastName && formik.touched.lastName ?
          (<ValidationMessage>{formik.errors.lastName}</ValidationMessage>) :
          null}
        <TextInput
          id='lastName'
          type='text'
          name='lastName'
          value={formik.values.lastName}
          onChange={formik.handleChange}
          placeholder={PLACEHOLDER.SURNAME}
        />
      </Label>
      <Label htmlFor="email">
        <LabelText>E-mail</LabelText>
        {formik.errors.email && formik.touched.email ?
          (<ValidationMessage>{formik.errors.email}</ValidationMessage>) :
          null}
        <TextInput
          id='email'
          type='text'
          name='email'
          value={formik.values.email}
          onChange={formik.handleChange}
          placeholder={PLACEHOLDER.EMAIL}
        />
      </Label>
      <Label htmlFor="gender">
        <LabelText>Пол</LabelText>
        {formik.errors.gender && formik.touched.gender ?
          (<ValidationMessage>{formik.errors.gender}</ValidationMessage>) :
          null}
        <Dropdown
          id='gender'
          name='gender'
          value={formik.values.gender}
          onChange={formik.handleChange}>
          <option value="male">Мужчина</option>
          <option value="female">Женщина</option>
        </Dropdown>
      </Label>
      <Label htmlFor="birthDate">
        <LabelText>День рождения</LabelText>
        {formik.errors.birthDate && formik.touched.birthDate ?
          (<ValidationMessage>{formik.errors.birthDate}</ValidationMessage>) :
          null}
        <TextInput
          id='birthDate'
          type='date'
          name='birthDate'
          value={formik.values.birthDate}
          onChange={formik.handleChange}
        />
      </Label>
      <div>
        <ActionButton onClick={formik.submitForm} style={{margin: 0}}>готово</ActionButton>
      </div>
    </Grid>
  );
}
