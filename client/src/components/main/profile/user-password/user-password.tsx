import React, {useState} from 'react';
import {useFormik} from 'formik';
import {useSelector} from 'react-redux';
import * as Yup from 'yup';
import {Form} from './user-password.style';
import {toast} from 'material-react-toastify';
import {TODO_ANY} from '../../../../types/misc';
import {toastConfig} from '../../../../theme/common';
// @ts-ignore
import {ReactComponent as EyeIcon} from '../../../../resources/eye.svg';
import {
  Label,
  LabelText,
  ValidationMessage,
  ActionButton,
  PasswordInput,
} from '../../../../theme/form';
import {
  passwordValidationRules,
  passwordConfirmValidationRules,
} from '../../../../config/form';
import {env} from '../../../../config/environment';
import {ENDPOINT} from '../../../../config/constants';

const axios = require('axios');
const UserPasswordSchema = Yup.object().shape({
  currentPassword: passwordValidationRules,
  newPassword: passwordValidationRules,
  newPasswordConfirmation: passwordConfirmValidationRules('newPassword'),
})
;

/**
 * A component allowing authorised user to manage his password.
 * @remarks Appears only in user profile.
 * @returns JSX.Element
 */
export default function UserPassword(): JSX.Element {
  const auth = useSelector((state: TODO_ANY) => state.auth);
  const [isProtected, setIsProtected] = useState(true);
  const formik = useFormik({
    initialValues: {
      currentPassword: '',
      newPassword: '',
      newPasswordConfirmation: '',
    },
    validationSchema: UserPasswordSchema,
    onSubmit: (values, {resetForm, setFieldError}) => {
      axios.put(
          `${env.apiBaseUrl}${ENDPOINT.PASSWORD}`,
          {
            oldPassword: values.currentPassword,
            newPassword: values.newPassword,
          },
          {
            headers: {
              'Authorization': auth.token,
            }})
          .then(() => {
            resetForm();
            toast.success('Password updated', toastConfig);
          })
          .catch((e : TODO_ANY) => {
            if (!!e.response && e.response.data.field === 'Password') {
              setFieldError('currentPassword', e.response.data.message);
            }
            toast.error('Password update failed', toastConfig);
          });
    },
  });

  return (
    <Form action="#" onSubmit={formik.handleSubmit}>
      <Label htmlFor="currentPassword">
        <LabelText>Пароль</LabelText>
        {formik.errors.currentPassword && formik.touched.currentPassword ?
          (<ValidationMessage>{formik.errors.currentPassword}</ValidationMessage>) :
          null}
        <PasswordInput hidePassword={isProtected}>
          <input
            id='currentPassword'
            type={isProtected ? 'password' : 'text'}
            name='currentPassword'
            value={formik.values.currentPassword}
            onChange={formik.handleChange}
            placeholder='*************'
          />
          <EyeIcon
            onMouseDown={() => setIsProtected(false)}
            onMouseUp={() => setIsProtected(true)}/>
        </PasswordInput>
      </Label>
      <Label htmlFor="newPassword">
        <LabelText>Новый пароль</LabelText>
        {formik.errors.newPassword && formik.touched.newPassword ?
          (<ValidationMessage>{formik.errors.newPassword}</ValidationMessage>) :
          null}
        <PasswordInput hidePassword={isProtected}>
          <input
            id='newPassword'
            type={isProtected ? 'password' : 'text'}
            name='newPassword'
            value={formik.values.newPassword}
            onChange={formik.handleChange}
            placeholder='*************'
          />
          <EyeIcon
            onMouseDown={() => setIsProtected(false)}
            onMouseUp={() => setIsProtected(true)}/>
        </PasswordInput>
      </Label>
      <Label htmlFor="newPasswordConfirmation">
        <LabelText>Подтверждение пароля</LabelText>
        {formik.errors.newPasswordConfirmation && formik.touched.newPasswordConfirmation ?
          (<ValidationMessage>{formik.errors.newPasswordConfirmation}</ValidationMessage>) :
          null}
        <PasswordInput hidePassword={isProtected}>
          <input
            id='newPasswordConfirmation'
            type={isProtected ? 'password' : 'text'}
            name='newPasswordConfirmation'
            value={formik.values.newPasswordConfirmation}
            onChange={formik.handleChange}
            placeholder='*************'
          />
          <EyeIcon
            onMouseDown={() => setIsProtected(false)}
            onMouseUp={() => setIsProtected(true)}/>
        </PasswordInput>
      </Label>
      <div>
        <ActionButton onClick={formik.submitForm} style={{margin: 0}}>готово</ActionButton>
      </div>
    </Form>
  );
}
