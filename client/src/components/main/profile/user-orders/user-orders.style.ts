import styled from 'styled-components';
import {PALETTE} from '../../../../theme/theme';
import {ORDER_STATUS} from '../../../../types/order';
// @ts-ignore
import {ReactComponent as BoxIcon} from '../../../../resources/box.svg';

export const EmptyContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const EmptyBox = styled(BoxIcon)`
  fill: ${PALETTE.LIGHT_GRAY};
  height: 256px; width: 256px;
`;

export const Table = styled.div`
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: auto;
`;

export const TableHeader = styled(Table)`
  background-color: ${PALETTE.PALE_SKY};
  font-weight: bold;
  color: ${PALETTE.WHITE};
  padding: 20px 0;
`;

export const TableRow = styled(Table)`
  border-bottom: 1px solid ${PALETTE.LIGHT_GRAY};
  
  &:hover {
    background-color: ${PALETTE.LIGHT_GRAY};
  }
`;

export const TableCell = styled.div`
  padding: 20px 0;
  text-align: center;
`;

export const TableCellPrice = styled(TableCell)`
  &:after {
    content: " грн";
  }
`;

interface ITableCellStatusProps {
  status: ORDER_STATUS
}

export const TableCellStatus = styled(TableCell)<ITableCellStatusProps>`
  color: ${(props) => {
    switch (props.status) {
      case ORDER_STATUS.PENDING: return PALETTE.NOBEL;
      case ORDER_STATUS.COMPLETED: return PALETTE.APPLE;
      case ORDER_STATUS.CANCELLED: return PALETTE.ALIZARIN_CRIMSON;
    }
  }};
`;
