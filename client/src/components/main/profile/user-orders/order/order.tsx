import React from 'react';
import {TableRow, TableCell, TableCellPrice, TableCellStatus} from '../user-orders.style';
import {IOrder} from '../../../../../types/order';

interface IOrderProps {
  target: IOrder
}

/**
 * Represents order at user profile.
 * @prop target - An object describing an order to be rendered.
 * @returns JSX.Element
 */
export default function Order(props: IOrderProps): JSX.Element {
  const {orderId, products, total, status, date} = props.target;

  /**
   * Helper function intended to generate properly formatted date string.
   * @returns string
   */
  function generateDataString(dateString: number): string {
    const dateObject = new Date(dateString);
    return `${dateObject.getDate()}.${dateObject.getMonth() + 1}.${dateObject.getFullYear()}`;
  }

  return (
    <TableRow key={orderId}>
      <TableCell>{orderId}</TableCell>
      <TableCell>{products.length}</TableCell>
      <TableCell>{generateDataString(date)}</TableCell>
      <TableCellPrice>{total}</TableCellPrice>
      <TableCellStatus status={status}>{status}</TableCellStatus>
    </TableRow>
  );
}
