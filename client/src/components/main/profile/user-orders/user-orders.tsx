import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import {PALETTE} from '../../../../theme/theme';
import {TableHeader, EmptyContainer, EmptyBox} from './user-orders.style';
import ClimbingBoxLoader from 'react-spinners/ClimbingBoxLoader';
import {AxiosResponse} from 'axios';
import {toast} from 'material-react-toastify';
import {toastConfig} from '../../../../theme/common';
import {TODO_ANY} from '../../../../types/misc';
import Order from './order/order';
import ErrorPage from '../../../misc/error-page/error-page';
import {env} from '../../../../config/environment';
import {ENDPOINT} from '../../../../config/constants';

const md5 = require('md5');
const axios = require('axios');

/**
 * User profile order tab controller
 * @returns JSX.Element
 */
export default function UserOrders(): JSX.Element {
  const [isLoading, setIsLoading] = useState(true);
  const [orders, setOrders] = useState([]);
  const auth = useSelector((state: TODO_ANY) => state.auth);

  useEffect(() => {
    axios.get(
        `${env.apiBaseUrl}${ENDPOINT.ORDERS}/${auth.id}`)
        .then((res: AxiosResponse) => setOrders(res.data))
        .catch(() => toast.warning('Orders fetching failed', toastConfig))
        .finally(() => setIsLoading(false));
  }, []);

  /**
   * Helper function which renders order table headers
   * @returns Array of JSX.Element
   */
  function renderHeaders(): JSX.Element[] {
    const tableColumns = ['Номер Заказа', 'К-во товаров', 'Дата', 'Сума', 'Статус'];

    return tableColumns.map((item) => {
      return <div key={md5(item)}>{item}</div>;
    });
  }

  if (isLoading) {
    return <EmptyContainer>
      <ClimbingBoxLoader color={PALETTE.APPLE}/>
    </EmptyContainer>;
  }

  if (!orders.length) {
    return <ErrorPage
      image={<EmptyBox/>}
      header='Whoa, you have no orders!'
      message='How about you go surf our great store?'
    />;
  }

  return (
    <div>
      <TableHeader>
        {renderHeaders()}
      </TableHeader>
      {orders.map((item) => <Order key={md5(item)} target={item}/>)}
    </div>
  );
}
