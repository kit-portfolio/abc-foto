import styled from 'styled-components';

export const Section = styled.section`
  flex-grow: 1;
  display: flex;
  align-items: center;
  flex-direction: column;
  
`;
