import styled from 'styled-components';
import {PALETTE, FONT_SIZE} from '../../../../theme/theme';

export const Container = styled.div`
  width: 480px;
`;

export const Header = styled.h3`
  font-size: ${FONT_SIZE.SUB_HEADER};
  text-align: left;
  color: ${PALETTE.WHITE};
  background-color: ${PALETTE.PALE_SKY};
  box-sizing: border-box;
  padding: 10px 60px;
  margin-bottom: 0;
`;

export const ContentBox = styled.div`
  box-sizing: border-box;
  padding: 60px;
  background-color: ${PALETTE.LIGHT_GRAY};
`;

export const ItemsBox = styled.div`
  overflow-y: scroll;
  height: 300px;
  margin-bottom: 80px;

  /* width */
  ::-webkit-scrollbar {
    width: 7px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    border-radius: 5px;
    background: ${PALETTE.PALE_SKY};
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`;

// TODO: remove when cart item component will be ready
export const Placeholder = styled.div`
  border: 1px dotted ${PALETTE.PALE_SKY};
  background-color: ${PALETTE.NOBEL};
  height: 95px;
  margin-bottom: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 20px;
  font-weight: bold;
`;

export const Shipping = styled.h3`
  ${FONT_SIZE.SUB_HEADER};
  text-align: left;
  color: ${PALETTE.NOBEL};
  font-weight: normal;
`;

export const OrderTotal = styled.h2`
  ${FONT_SIZE.HEADER};
  text-align: left;
`;
