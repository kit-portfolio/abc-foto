import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import * as Yup from 'yup';
import 'yup-phone';
import {useFormik} from 'formik';
import {Grid, Header} from './checkout.style';
import CheckoutForm from './checkout-form/checkout-form';
import CartWidget from './cart-widget/cart-widget';
import {
  nameValidationRules,
  phoneValidationRules,
  emailValidationRules,
  submitStub,
} from '../../../config/form';
import ErrorBoundary from '../../misc/error-boundary/error-boundary';
import {BREADCRUMBS_ACTION} from '../../../store/breadcrumbs/breadcrumbs.actions';


const OrderSchema = Yup.object().shape({
  name: nameValidationRules,
  phone: phoneValidationRules,
  email: emailValidationRules,
});

/**
 * Checkout component implements checkout page
 * @returns JSX.Element
 */
export default function Checkout(): JSX.Element {
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      name: '',
      phone: '',
      email: '',
      shipping: {
        type: '',
        address: '',
      },
    },
    validationSchema: OrderSchema,
    onSubmit: (values, {resetForm}) => submitStub(values, resetForm),
  });

  useEffect(() => {
    dispatch({type: BREADCRUMBS_ACTION.CHECKOUT});
  });

  return (
    <ErrorBoundary>
      <Grid>
        <Header>Оформление заказа</Header>
        <CheckoutForm formController={formik}/>
        <CartWidget formController={formik}/>
      </Grid>
    </ErrorBoundary>
  );
}
