import styled from 'styled-components';
import {FONT_SIZE} from '../../../../theme/theme';

export const Header = styled.h3`
  ${FONT_SIZE.SUB_HEADER};
  text-align: left;
`;
