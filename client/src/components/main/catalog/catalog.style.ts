import styled from 'styled-components';
import {FONT_SIZE, PALETTE} from '../../../theme/theme';
import {Label, ActionButton} from '../../../theme/form';

export const Filter = styled.div`
  width: 16%;
`;

export const FilterHeader = styled.p`
  ${FONT_SIZE.TEXT};
  font-weight: 600;
`;

export const FilterLabel = styled(Label)`
  ${FONT_SIZE.TEXT};
`;

export const ProductsContainer = styled.div`
  display: grid;
  justify-content: center;
  grid-template-columns: repeat(3, 280px);
  grid-template-rows: repeat(3, 1fr);
  grid-row-gap: 80px;
  grid-column-gap: 20px;
`;

export const Container = styled.div`
  width: 1200px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const Header = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  align-items: center;
  ${FONT_SIZE.SUB_HEADER};
  text-transform: uppercase;
  font-weight: 600;
`;

export const Button = styled(ActionButton)`
  width: 100%;
  ${FONT_SIZE.TEXT};
  text-transform: none;
  padding: 10px 15px ;
`;

export const ProductNotFound = styled.div`
  ${FONT_SIZE.HEADER};
  color: ${PALETTE.NOBEL};
  width: 84%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
