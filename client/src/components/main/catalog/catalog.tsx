import React, {useEffect, useState} from 'react';
import ProductCard from '../../misc/product-card/product-card';
import {TODO_ANY} from '../../../types/misc';
import ClipLoader from 'react-spinners/ClipLoader';
import {
  Container,
  Filter,
  FilterHeader,
  FilterLabel,
  Header,
  ProductsContainer,
  Button,
  ProductNotFound,
} from './catalog.style';
import {fetchData} from '../../../config/fetch';
import {IProduct} from '../../../types/product';
import {useHistory} from 'react-router';
import ProductPage from '../product-page/product-page';
import Checkbox, {CheckboxProps} from '@material-ui/core/Checkbox';
import {PALETTE} from '../../../theme/theme';
import withStyles from '@material-ui/core/styles/withStyles';
import {env} from '../../../config/environment';
import {ENDPOINT} from '../../../config/constants';

const GreenCheckbox = withStyles({
  root: {
    'color': PALETTE.APPLE,
    '&$checked': {
      color: PALETTE.APPLE,
    },
  },
  checked: {},
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

/**
 * Catalog - component displaying product cards
 * @remarks Fetches items from api/products.
 * @returns JSX.Element
 */
export default function Catalog(): JSX.Element {
  const history = useHistory();
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [items, setItems] = useState<IProduct[]>([]);
  const md5 = require('md5');
  const [userFilterManufacturer, setUserFilterManufacturer] = useState<string[]>([]);
  const [wantToFilterByManufacturer, setWantToFilterByManufacturer] = useState<boolean>(false);

  const checkboxController = (item: string) => {
    const i = userFilterManufacturer.indexOf(item);
    const cloneState = Object.assign([], userFilterManufacturer);
    if (i === -1) {
      cloneState.push(item);
      setUserFilterManufacturer(cloneState);
    } else {
      cloneState.splice(i, 1);
      setUserFilterManufacturer(cloneState);
    }
  };

  useEffect(() => {
    fetchData({
      href: `${env.apiBaseUrl}${ENDPOINT.PRODUCTS}`,
      setItems,
      setIsLoaded,
    });
  }, []);

  useEffect(() => {
    setUserFilterManufacturer([]);
    setWantToFilterByManufacturer(false);
  }, [history.location.pathname]);

  /**
   * contentController - component that filters content of catalog, depending on href
   * @returns JSX.Element
   */
  function contentController(): JSX.Element {
    const SRC: string[] = history.location.pathname.split('/');
    // Delete first el "" and second el "catalog"
    SRC.splice(0, 2);
    const param = {
      category: SRC[0],
      type: SRC[1],
      _id: SRC[2],
    };
    let filteredContent = items;
    if (param._id) {
      return <ProductPage id={param._id}/>;
    }
    if (param.category) {
      filteredContent = filteredContent.filter((item) => item.category === param.category);
    }
    if (param.type) {
      filteredContent = filteredContent.filter((item) => item.type === param.type);
    }
    const manufacturerSet = Array.from(new Set(filteredContent.map((item) => item.manufacturer)));

    const checkboxGenerator = () =>{
      return (
        <>
          {manufacturerSet.map((item: string) => {
            return (
              <FilterLabel key={md5(item)}>
                <GreenCheckbox onChange={()=> {
                  checkboxController(item);
                }}
                checked={userFilterManufacturer.includes(item)}
                />
                {item}
              </FilterLabel>
            );
          })}
        </>
      );
    };

    if (wantToFilterByManufacturer) {
      filteredContent = filteredContent.filter((item) => userFilterManufacturer.includes(item.manufacturer));
    }

    const content = () => {
      return (
        <>
          {
            !!filteredContent.length && <ProductsContainer>
              {filteredContent.map((item: TODO_ANY): JSX.Element => {
                return (
                  <ProductCard name={item.name}
                    manufacturer={item.manufacturer}
                    image={item.images[0].image}
                    price={item.price}
                    stockStatus={item.quantity !== 0}
                    key={item._id}
                    category={item.category}
                    type={item.type}
                    id={item._id}
                  />
                );
              })}
            </ProductsContainer>
          }
          {
            !filteredContent.length && <ProductNotFound>Продукт не найден</ProductNotFound>
          }
        </>

      );
    };

    return (
      <Container>
        <Header>
          Каталог
        </Header>
        <Filter>
          <FilterHeader>
            По производителю
          </FilterHeader>
          {checkboxGenerator()}
          <Button onClick={() => setWantToFilterByManufacturer(!wantToFilterByManufacturer)}>
            {wantToFilterByManufacturer? 'Убрать фильтр' : 'Включить фильтр'}
          </Button>
        </Filter>
        {content()}
      </Container>
    );
  }

  if (!isLoaded) {
    return <ClipLoader/>;
  } else {
    return contentController();
  }
}
