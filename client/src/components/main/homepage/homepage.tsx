import React, {useEffect} from 'react';
import SaleAnnounce from './sale-announce/sale-announce';
import Cover from './cover/cover';
import ErrorBoundary from '../../misc/error-boundary/error-boundary';
import ProductCarousel from '../../misc/carousel/product-carousel';
import {BREADCRUMBS_ACTION} from '../../../store/breadcrumbs/breadcrumbs.actions';
import {useDispatch} from 'react-redux';
/**
 * Homepage component
 * @returns JSX.Element
 */
export default function Homepage(): JSX.Element {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({type: BREADCRUMBS_ACTION.CLEAR});
  }, []);

  return (
    <ErrorBoundary>
      <Cover/>
      <ProductCarousel/>
      <SaleAnnounce/>
    </ErrorBoundary>
  );
};
