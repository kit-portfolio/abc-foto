import React, {useState, useEffect} from 'react';
import {
  SliderContainer,
  SliderTrack,
  SwipeButton,
  DotsLine,
  SliderContent,
} from '../../../misc/carousel/default/default-carousel.style';
import {
  handleEventDirectionController,
  BUTTON_TYPE,
  ICONFIGURATION,
  dotsGenerator,
} from '../../../misc/carousel/default/function';
import ClipLoader from 'react-spinners/ClipLoader';
import {PALETTE} from '../../../../theme/theme';
import {fetchData} from '../../../../config/fetch';
import ICover from '../../../../types/cover';
import {env} from '../../../../config/environment';
import {ENDPOINT} from '../../../../config/constants';

const CONFIGURATION: ICONFIGURATION = {
  STRIDE_LENGTH: 1200,
  STEP: 1,
  SHIFT: '-3px',
  CONTENT_WIDTH: '1200px',
  ANIMATION_DURATION: '0.8s',
};

/**
 * Cover is a big slider located at the top of the Homepage.
 * @remarks Fetches images from api/cover
 * @returns JSX.Element
 */
export default function Cover(): JSX.Element {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [items, setItems] = useState<ICover[]>([]);
  const [position, setPosition] = useState<number>(0);
  const quantity: number = items.length;
  const maxDisplacement: number = (quantity - 1) * 1200;

  useEffect(() => {
    fetchData({
      href: `${env.apiBaseUrl}${ENDPOINT.COVER}`,
      setIsLoaded,
      setItems,
    });
  }, []);

  const ArrayOfImages = items.map((item: ICover): JSX.Element => {
    return (
      <SliderContent
        width={CONFIGURATION.CONTENT_WIDTH}
        key={item._id}
        onClick={() => alert(`Вы кликнули на слайд. Ссылка: ${item.link}`)}
        image={item.href}
        haveSelfHeight={false}/>
    );
  });

  if (!isLoaded) {
    return <ClipLoader/>;
  }

  return (
    <SliderContainer>
      <SwipeButton
        onClick={() => handleEventDirectionController({
          maxDisplacement,
          setPosition,
          type: BUTTON_TYPE.PREV,
          position,
          strideLength: CONFIGURATION.STRIDE_LENGTH,
          step: CONFIGURATION.STEP,
        })}
        left={CONFIGURATION.SHIFT}
        color={PALETTE.APPLE}/>
      <SwipeButton
        onClick={() => handleEventDirectionController({
          maxDisplacement,
          setPosition,
          type: BUTTON_TYPE.NEXT,
          position,
          strideLength: CONFIGURATION.STRIDE_LENGTH,
          step: CONFIGURATION.STEP,
        })}
        right={CONFIGURATION.SHIFT}
        color={PALETTE.APPLE}
        isRotating/>
      <DotsLine quantity={quantity}>
        {dotsGenerator({
          setPosition,
          items,
          position,
        })
        }
      </DotsLine>
      <SliderTrack position={position} duration={CONFIGURATION.ANIMATION_DURATION}>
        {ArrayOfImages}
      </SliderTrack>
    </SliderContainer>
  );
}
