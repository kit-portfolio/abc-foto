import React from 'react';
import {Link} from 'react-router-dom';
import {Container, Header, SalesGrid, SalePoster} from './sale-announce.style';
import {ContentBox} from '../../../../theme/common';
import {data as saleAnnouncements} from '../../../../mock/sales-announcements.json';

/**
 * Sale announce block renders a block of sale-announce posters on homepage
 * @returns JSX.Element
 */
export default function SaleAnnounce(): JSX.Element {
  return (
    <Container>
      <ContentBox>
        <Header>акции и спецпредложения</Header>
        <SalesGrid>
          {saleAnnouncements.map((item, index) => {
            return <Link to={item.href} key={item.id} style={{gridArea: `sale-${index + 1}`}}>
              <SalePoster src={`img/sale-announcement-posters/${item.title}`} alt={item.desc}/>
            </Link>;
          })}
        </SalesGrid>
      </ContentBox>
    </Container>
  );
}
