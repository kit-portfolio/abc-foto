import styled from 'styled-components';
import {FONT_SIZE, PALETTE} from '../../../theme/theme';
import {darken} from 'polished';

export const Environment = styled.div`
   background: ${PALETTE.APPLE};
`;

export const Container = styled.div`
   max-width: 1200px;
   display: flex;
   margin: 15px auto 0;
`;

const DefaultCategory = styled.div`
  text-align: center;
  font-size: ${FONT_SIZE.TEXT};
  color: ${PALETTE.WHITE};
  cursor:pointer;
  padding: 10px 0;
  position: relative;
  flex-grow: 1;
    
  &:hover{
    background: ${darken(0.05, PALETTE.APPLE)};
  }  
  }
`;

export const AdditionalCategory = styled.div`
  z-index: 15;
  background: ${darken(0.05, PALETTE.APPLE)};
  display: none;
`;

export const Category = styled(DefaultCategory)`
  &:hover ${AdditionalCategory}{
      display: flex;
      flex-direction: column;
      box-sizing: border-box;
      padding: 10px 0;
      width: 100%;
      position: absolute;
      left: 0;
      top: 38px;
      & div:hover{
      font-weight: 600;
      background: ${darken(0.07, PALETTE.APPLE)};
      }
    }
`;

