import React from 'react';
import {Category, Container, Environment, AdditionalCategory} from './categories.style';
import {useHistory} from 'react-router';
import {URN} from '../../../config/constants';
import {useDispatch} from 'react-redux';
import {BREADCRUMBS_ACTION} from '../../../store/breadcrumbs/breadcrumbs.actions';

/**
 * Bar providing user with product categories navigation interface.
 * @returns JSX.Element
 */
export default function Categories(): JSX.Element {
  const history = useHistory();
  const dispatch = useDispatch();
  const redirect = (event: React.MouseEvent<HTMLDivElement>, arg: string) => {
    history.push(arg);
  };

  return (
    <Environment>
      <Container>
        <Category onClick={(event) => {
          dispatch({type: BREADCRUMBS_ACTION.CLEAR});
          redirect(event, `${URN.CATALOG}`);
        }}>
          Все товары
        </Category>
        <Category>
          Цифровая техника
          <AdditionalCategory>
            <Category onClick={(event) => {
              dispatch({type: BREADCRUMBS_ACTION.CLEAR});
              redirect(event, `${URN.CATALOG}/photo/camera`);
            }}>
              Фотоаппараты
            </Category>
            <Category onClick={(event) => {
              dispatch({type: BREADCRUMBS_ACTION.CLEAR});
              redirect(event, `${URN.CATALOG}/photo/video-camera`);
            }}>
              Видеокамеры
            </Category>
            <Category onClick={(event) => {
              dispatch({type: BREADCRUMBS_ACTION.CLEAR});
              redirect(event, `${URN.CATALOG}/externalHardDrive`);
            }}>
              Жесткие диски
            </Category>
          </AdditionalCategory>
        </Category>
        <Category onClick={(event) => {
          dispatch({type: BREADCRUMBS_ACTION.CLEAR});
          redirect(event, `${URN.CATALOG}/photo/lens`);
        }}>
          Оптика для фото
        </Category>
        <Category>
          Аксессуары
          <AdditionalCategory>
            <Category onClick={(event) => {
              dispatch({type: BREADCRUMBS_ACTION.CLEAR});
              redirect(event, `${URN.CATALOG}/photo/paper`);
            }}>
              Фотобумага
            </Category>
            <Category onClick={(event) => {
              dispatch({type: BREADCRUMBS_ACTION.CLEAR});
              redirect(event, `${URN.CATALOG}/memory%20cards`);
            }}>
              Карта памяти
            </Category>
          </AdditionalCategory>
        </Category>
        <Category onClick={(event) => {
          dispatch({type: BREADCRUMBS_ACTION.CLEAR});
          redirect(event, `${URN.CATALOG}/photo/case`);
        }}>
          Чехлы
        </Category>
        <Category onClick={(event) => {
          dispatch({type: BREADCRUMBS_ACTION.CLEAR});
          redirect(event, `${URN.CATALOG}/tripod`);
        }}>
          Штативы
        </Category>
        <Category onClick={(event) => {
          dispatch({type: BREADCRUMBS_ACTION.CLEAR});
          redirect(event, `${URN.CATALOG}/photo/frame`);
        }}>
          Фоторамки
        </Category>
      </Container>
    </Environment>
  );
};
