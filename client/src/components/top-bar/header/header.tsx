import React from 'react';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import Logo from '../../misc/logo/logo';
import {
  ContentBoxExtended,
  Form,
} from './header.style';
// @ts-ignore
import {ReactComponent as SearchButton} from '../../../resources/search.svg';
import UserWidget from './user-widget/user-widget';
import CartWidget from './cart-widget/cart-widget';
import Contacts from './contacts/contacts';
import {PLACEHOLDER, submitStub} from '../../../config/form';

const SearchQuerySchema = Yup.object().shape({
  query: Yup.string().required('Required'),
});

/**
 * Header is the main functional area of TopBar
 * @returns JSX.Element
 */
export default function Header(): JSX.Element {
  const formik = useFormik({
    initialValues: {
      query: '',
    },
    validationSchema: SearchQuerySchema,
    onSubmit: (values, {resetForm}) => submitStub(values, resetForm),
  });

  return (
    <div>
      <ContentBoxExtended>
        <Logo isColored/>
        <Form onSubmit={formik.handleSubmit}>
          <input
            name='query'
            type='text'
            value={formik.values.query}
            onChange={formik.handleChange}
            placeholder={PLACEHOLDER.SEARCH}/>
          <SearchButton onClick={formik.submitForm}/>
        </Form>
        <Contacts/>
        <UserWidget/>
        <CartWidget/>
      </ContentBoxExtended>
    </div>
  );
}
