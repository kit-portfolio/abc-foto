import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useHistory} from 'react-router';
import {WidgetLabel} from '../header.style';
import {UserLogo, UserAvatar, UserWidgetWrapper} from './user-widget.style';
import {TODO_ANY} from '../../../../types/misc';
import {openAuthModal} from '../../../../store/modal-controller/modal-controller.actions';
import {URN} from '../../../../config/constants';

/**
 * User - button that responsible for information about user-widget authorization.
 * @remarks Calls up an authorization window or personal account.
 * @returns JSX.Element
 */
function UserWidget(): JSX.Element {
  const history = useHistory();
  const dispatch = useDispatch();
  const auth = useSelector((state:TODO_ANY) => state.auth);
  const user = useSelector((state:TODO_ANY) => state.user);

  /**
   * This method conditionally renders user avatar depending on auth state
   * @returns JSX.Element
   */
  function renderAvatar(): JSX.Element {
    if (auth.isLoggedIn && user.avatarUrl) {
      return <UserAvatar src={user.avatarUrl}/>;
    } else if (auth.isLoggedIn) {
      return <UserLogo isLoggedIn/>;
    } else {
      return <UserLogo/>;
    }
  }

  return (
    <UserWidgetWrapper
      onClick={() => {
          (auth.isLoggedIn) ?
            history.push(URN.PROFILE):
            dispatch(openAuthModal());
      }}>
      {renderAvatar()}
      <WidgetLabel>{user.firstName ? user.firstName : 'Вход'}</WidgetLabel>
    </UserWidgetWrapper>
  );
}

export default UserWidget;
