import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {
  Container,
  Selector,
  SelectorChevron,
  Dropdown,
  PhoneNumberContainer,
  PhoneImage,
  PhoneNumber,
} from './contacts.style';
import {IContact} from '../../../../types/custom';
import {env} from '../../../../config/environment';

/**
 * Contacts is a module allowing user to get in touch with vendor in the most comfortable way.
 * @remarks Appears only in Header
 * @returns JSX.Element
 */
export default function Contacts(): JSX.Element {
  const [contacts, setContacts] = useState([]);
  const [isDropdownShown, setIsDropdownShown] = useState(false);

  useEffect(() => {
    axios.get(`${env.apiBaseUrl}/contacts?enabledOnly=true`)
        .then((res) => setContacts(res.data));
  }, []);

  /**
   * Contact dropdown click handler
   * @returns void
   */
  function handleContactClick(): void {
    setIsDropdownShown(!isDropdownShown);
  }

  /**
   * Helper function intended to handle contacts dropdown conditional render
   * @returns JSX.Element | null
   */
  function renderDropdown(): JSX.Element | null {
    if (!isDropdownShown) return null;
    else {
      return <Dropdown>
        {contacts.map((item: IContact) => {
          return <PhoneNumberContainer key={item._id}>
            <PhoneImage src={item.image}/>
            <PhoneNumber href={item.href}>{item.label}</PhoneNumber>
          </PhoneNumberContainer>;
        })}
      </Dropdown>;
    }
  }

  return <>
    <Container onClick={() => handleContactClick()}>
      <Selector
        value="call us"
        disabled/>
      <SelectorChevron/>
      {renderDropdown()}
    </Container>
  </>;
}
