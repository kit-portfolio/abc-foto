import styled from 'styled-components';
import {ContentBox, simpleForm} from '../../../theme/common';
import {FONT_SIZE} from '../../../theme/theme';

export const ContentBoxExtended = styled(ContentBox)`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Form = styled(simpleForm)`
  width: 450px;
  margin-left: 30px;
`;

export const WidgetWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 150px;
  cursor: pointer;
`;

export const widgetPictogramDimensions = {
  height: '41px',
  width: '41px',
};

export const WidgetLabel = styled.h2`
  ${FONT_SIZE.TEXT};
`;
