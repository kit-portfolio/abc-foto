import React from 'react';
import {useSelector} from 'react-redux';
import {Container, Breadcrumb} from './breadcrumbs.style';
import {IBreadcrumb} from '../../../store/breadcrumbs/breadcrumbs.actions';
import {URN} from '../../../config/constants';
import {TODO_ANY} from '../../../types/misc';

const md5 = require('md5');

/**
 * Breadcrumbs component displays site hierarchy on certain pages.
 * @remarks This component depends on breadcrumbs state.
 * If there are no entities in breadcrumbs array, component will not be rendered.
 * @returns JSX.Element | null
 */
export default function Breadcrumbs(): JSX.Element | null {
  const breadcrumbs: Array<IBreadcrumb> = useSelector((state: TODO_ANY) => state.breadcrumbs);

  if (!breadcrumbs.length) {
    return null;
  }

  return <Container>
    <Breadcrumb to={URN.HOMEPAGE}>Главная</Breadcrumb>
    {breadcrumbs.map((item) => <Breadcrumb
      key={md5(item)}
      to={(item.href) ? item.href : '#'}
      isActive={!item.href}
    >{item.label}</Breadcrumb>)}
  </Container>;
}
