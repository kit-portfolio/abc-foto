import styled from 'styled-components';
import {ContentBox} from '../../../theme/common';
import {Link} from 'react-router-dom';
import {FONT_SIZE, PALETTE} from '../../../theme/theme';

export const Container = styled(ContentBox)`
  margin: 15px auto;
  text-align: left;
`;

interface IBreadcrumb {
  isActive?: boolean
}

/**
 * Helper function intended to form Breadcrumb styles related to 'isActive' property.
 * @returns string
 */
function getHoverStyles(isActive: boolean): string {
  const color = `color: ${(isActive) ? PALETTE.PALE_SKY : PALETTE.BLACK};`;
  const cursor = `cursor: ${(isActive) ? 'default' : 'pointer'};`;
  const hover = (isActive) ? '' : `&:hover {color: ${PALETTE.APPLE}};`;
  return color + cursor + hover;
}

export const Breadcrumb = styled(Link)<IBreadcrumb>`
  ${FONT_SIZE.SMALL_TEXT};
  text-decoration: none;
  user-select: none;
  ${(props) => getHoverStyles(!!props.isActive)};
  
  &::after {
    content: ">";
    margin: 0 5px;
  }

  &:last-of-type::after {
    content: "";
  }
`;
