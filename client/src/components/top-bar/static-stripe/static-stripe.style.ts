import styled from 'styled-components';
import {Link as ReactLink} from 'react-router-dom';
import {PALETTE} from '../../../theme/theme';

export const Section = styled.div`
  background-color: ${PALETTE.LIGHT_GRAY};
  display: block;
  width: 100%;
`;

export const Link = styled(ReactLink)`
  display: inline-block;
  color: ${PALETTE.BLACK};
  text-decoration: none;
  padding: 5px 15px;
  font-size: 12px;
  &:hover{
    background-color: ${PALETTE.NOBEL};
  }
`;
