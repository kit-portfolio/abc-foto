import frisby from 'frisby';
import {URN} from '../../config/constants';
import * as mock from './mocks/root';

const baseURL = 'http://localhost:5000'; // TODO: get from config?

describe('Seeding contacts', function() {
  mock.contact.map((item) => {
    it(`Creating "${item.label}" contact`, function() {
      return frisby
          .post(`${baseURL}${URN.CONTACT}`, item)
          .expect('status', 201);
    });
  });
});

describe('Seeding subscriptions', function() {
  mock.subscribers.map((item) => {
    it(`Creating subscription for ${item}`, function() {
      return frisby
          .post(`${baseURL}${URN.SUBSCRIPTION}`, {
            'email': `${item}@example.com`,
          })
          .expect('status', 201);
    });
  });
});

describe('Seeding users', function() {
  mock.users.map((item) => {
    it(`Creating a user for ${item.lastName}`, function() {
      return frisby
          .post(`${baseURL}${URN.USER}`, item)
          .expect('status', 201)
          .then(async (res) => {
            if (res.json.firstName === 'Commander') {
              mock.orders.map((item) => {
                item.userId = res.json._id;
                return frisby
                    .post(`${baseURL}${URN.ORDER}`, item)
                    .expect('status', 201)
                    .catch((e) => console.log(e));
              });
            }
          });
    });
  });
});

describe('Seeding products', function() {
  const accessories = new Map();

  /**
   * Helper function intended to form a list of specific products accessories.
   */
  function updateAccessories(product) {
    (accessories.has(product.manufacturer)) ?
      accessories.get(product.manufacturer).push(product._id) :
      accessories.set(product.manufacturer, [product._id]);
  }

  describe('Seeding accessories', function() {
    mock.products.lenses.map((item) => {
      it(`Creating lens (${item.name})`, function() {
        return frisby
            .post(`${baseURL}${URN.PRODUCT}`, item)
            .expect('status', 201)
            .then((res) => updateAccessories(JSON.parse(res.body)));
      });
    });

    mock.products.memoryCards.map((item) => {
      it(`Creating memory card (${item.name})`, function() {
        return frisby
            .post(`${baseURL}${URN.PRODUCT}`, item)
            .expect('status', 201)
            .then((res) => updateAccessories(JSON.parse(res.body)));
      });
    });

    mock.products.paper.map((item) => {
      it(`Creating photo paper (${item.name})`, function() {
        return frisby
            .post(`${baseURL}${URN.PRODUCT}`, item)
            .expect('status', 201);
      });
    });
    mock.products.tripod.map((item) => {
      it(`Creating tripod (${item.name})`, function() {
        return frisby
            .post(`${baseURL}${URN.PRODUCT}`, item)
            .expect('status', 201);
      });
    });

    mock.products.cases.map((item) => {
      it(`Creating cases (${item.name})`, function() {
        return frisby
            .post(`${baseURL}${URN.PRODUCT}`, item)
            .expect('status', 201);
      });
    });

    mock.products.externalHardDrive.map((item) => {
      it(`Creating external hard driver (${item.name})`, function() {
        return frisby
            .post(`${baseURL}${URN.PRODUCT}`, item)
            .expect('status', 201);
      });
    });

    mock.products.frame.map((item) => {
      it(`Creating frame (${item.name})`, function() {
        return frisby
            .post(`${baseURL}${URN.PRODUCT}`, item)
            .expect('status', 201);
      });
    });
  });

  describe('Seeding cameras', function() {
    /**
     * Helper function intended to get accessories from a list.
     */
    function getAccessories(product) {
      if (accessories.has(product.manufacturer)) {
        return accessories.get(product.manufacturer);
      } else {
        return [];
      }
    }

    mock.products.cameras.map((item) => {
      it(`Creating "${item.name}"`, function() {
        return frisby
            .post(`${baseURL}${URN.PRODUCT}`, {
              ...item,
              accessories: getAccessories(item),
            })
            .expect('status', 201);
      });
    });
  });
});

describe('Seeding cover slides', function() {
  mock.covers.map((item) => {
    it(`Creating a slide for ${item.href}`, function() {
      return frisby
          .post(`${baseURL}${URN.COVER}`, item)
          .expect('status', 201);
    });
  });
});
