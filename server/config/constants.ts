export enum MODEL {
 CONTACT = 'contacts',
 COVER = 'cover',
 ORDER = 'orders',
 PRODUCT = 'products',
 SUBSCRIPTION = 'subscription',
 USER = 'users',
}

export enum URN {
  CONTACT = '/api/contacts',
  COVER = '/api/cover',
  INDEX = '/',
  ORDER = '/api/orders',
  PRODUCT = '/api/products',
  SUBSCRIPTION = '/api/subscription',
  USER = '/api/users',
}
