require('dotenv').config();

interface IKeys {
  mongoURI: string,
  secretOrKey: string,
  nodemailerUser?: string,
  nodemailerPassword?: string,
  nodemailerService?: string,
}

const prodKeys: IKeys = {
  mongoURI: 'placeholder',
  secretOrKey: 'placeholder',
  nodemailerUser: 'placeholder',
  nodemailerPassword: 'placeholder',
  nodemailerService: 'placeholder',
};

const devKeys = {
  mongoURI: 'mongodb://localhost:27017/abc-foto',
  secretOrKey: 'random, very-very secret string',
};

export default (process.env.NODE_ENV === 'production') ? prodKeys : devKeys;

