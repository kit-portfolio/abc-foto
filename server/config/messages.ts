export enum ENTITY {
  CONFIG = 'Config',
  CONTACT = 'Contact',
  ORDER = 'Order',
  PRODUCT = 'Product',
  SUBSCRIPTION = 'Subscription',
  USER = 'User',
  SLIDE = 'Slide',
}

export enum OPERATION {
  CREATE = 'creation',
  READ = 'reading',
  UPDATE = 'updating',
  DELETE = 'deletion',
}

export default {
  errors: {
    generic: {
      serverError: 'Error happened on server',
      permissionsLack: 'Your permissions is not enough to perform this task',
      environmentError: (entity: string) => `Environment property '${entity}' is incorrect or absent.`,
    },
    entitySpecific: {
      notFound: (entity: ENTITY): string => `${entity} not found.`,
      alreadyExists: (entity: ENTITY): string => `${entity} already exists.`,
    },
    fieldSpecific: {
      wrongPass: 'Wrong password.',
      passwordNoMatch: 'Password do not match',
      emailIsTaken: 'This email is already taken.',
    },
    validation: {
      entityIsRequired: 'This field is required.',
      invalidValue: (value: string): string => `${value} is invalid.`,
      incorrectType: (typeRequired: string): string => `This field value should be ${typeRequired}.`,
      incorrectLength: (s: number, e: number): string => `This field length must be between ${s} and ${e} characters.`,
      incorrectPattern: (pattern: string): string => `Allowed characters for this are: ${pattern}`,
    },
  },
  system: {
    serverLaunched: (port: string): string => `Server is up and running on port ${port}.`,
    databaseConnected: 'MongoDB connected.',
    wrongEndpoint: 'Not for this endpoint you looking are',
  },
  entityManipulation: {
    success: ( entity: ENTITY, operation: OPERATION): string => `${entity} ${operation} successful.`,
    error: ( entity: ENTITY, operation: OPERATION): string => `${entity} ${operation} error.`,
  },
  status: {
    passwordUpdated: 'Password is updated.',
  },
};
