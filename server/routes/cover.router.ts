import {Router as router} from 'express';
import {
  addNewSlide,
  getSlides,
  deleteSlide,
} from '../controllers/cover.controller';

export const cover: router = router({
  strict: true,
});

cover.post(
    '/',
    addNewSlide,
);

cover.delete(
    '/:id',
    deleteSlide,
);

cover.get(
    '/',
    getSlides,
);
