import {Router as router} from 'express';
import {createSubscription} from '../controllers/subscription.controller';

export const subscription: router = router({
  strict: true,
});

subscription.post(
    '/',
    createSubscription,
);
