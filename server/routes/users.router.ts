import {Router as router} from 'express';
import passport from 'passport';
import {
  createUser,
  loginUser,
  getUser,
  editUserInfo,
  updatePassword,
} from '../controllers/users.controller';


export const users: router = router({
  strict: true,
});

users.post(
    '/',
    createUser,
);

users.post(
    '/login',
    loginUser,
);

users.get(
    '/',
    passport.authenticate('jwt', {session: false}),
    // @ts-ignore
    getUser,
);

users.put(
    '/',
    passport.authenticate('jwt', {session: false}),
    // @ts-ignore
    editUserInfo,
);

users.put(
    '/password',
    passport.authenticate('jwt', {session: false}),
    // @ts-ignore
    updatePassword,
);

// TODO: Deal with stubs
