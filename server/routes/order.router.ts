import {Router as router} from 'express';
import {
  addNewOrder,
  updateOrder,
  getOrders,
  getOrderByUserId,
  deleteOrder,
} from '../controllers/order.controller';

export const order: router = router({
  strict: true,
});

order.post(
    '/',
    addNewOrder,
);

order.put(
    '/:id',
    updateOrder,
);

order.delete(
    '/:id',
    deleteOrder,
);

order.get(
    '/',
    getOrders,
);

order.get(
    '/:userId',
    getOrderByUserId,
);
