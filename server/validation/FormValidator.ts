import validator from 'validator';

interface IValidationStatus {
  isValid: boolean;
  errors?: {};
}
/**
 * FormValidator class
 */
export class FormValidator {
  public validations:any;

  /**
   * Constructor
   */
  constructor(validations) {
    this.validations = validations;
  }

  /**
   * Validate data provided
   */
  validate(data) {
    const validation = this.valid();
    validation.errors = {};

    this.validations.forEach((rule) => {
      const fieldValue = data[rule.field].toString();
      const args = rule.args || [];
      const validationMethod =
        typeof rule.method === 'string' ? validator[rule.method] : rule.method;

      if (validationMethod(fieldValue, ...args, data) !== rule.validWhen) {
        validation.errors = {
          ...validation.errors,
          [rule.field]: rule.message,
        };
        validation.isValid = false;
      }
    });

    return validation;
  }

  /**
   * Method verifying if given value is empty.
   * @returns boolean
   */
  valid(): IValidationStatus {
    return {isValid: true};
  }

  /**
   * Check validity of given value
   */
  static checkValidity(formValidationRules, currentFormInputTypes) {
    return formValidationRules.filter((rule) =>
      currentFormInputTypes.includes(rule.field),
    );
  }

  /**
   * Method verifying if given value is empty.
   */
  static isEmpty(value) {
    return (
      value === undefined ||
      value === null ||
      (typeof value === 'object' && Object.keys(value).length === 0) ||
      (typeof value === 'string' && value.trim().length === 0)
    );
  }
}
