import {FormValidator} from './FormValidator';
import messages from '../config/messages';

// Rules for all form fields for validation
export const formValidationRules = [
  {
    field: 'firstName',
    method: FormValidator.isEmpty,
    validWhen: false,
    message: messages.errors.validation.entityIsRequired,
  },
  {
    field: 'firstName',
    method: 'matches',
    validWhen: true,
    args: [/^[a-zA-Zа-яА-Я]+$/],
    message: messages.errors.validation.incorrectPattern('a-z, A-Z, а-я, А-Я'),
  },
  {
    field: 'firstName',
    method: 'isLength',
    validWhen: true,
    args: [{min: 2, max: 25}],
    message: messages.errors.validation.incorrectLength(2, 25),
  },
  {
    field: 'lastName',
    method: FormValidator.isEmpty,
    validWhen: false,
    message: messages.errors.validation.entityIsRequired,
  },
  {
    field: 'lastName',
    method: 'matches',
    validWhen: true,
    args: [/^[a-zA-Zа-яА-Я]+$/],
    message: messages.errors.validation.incorrectPattern('a-z, A-Z, а-я, А-Я'),
  },
  {
    field: 'lastName',
    method: 'isLength',
    validWhen: true,
    args: [{min: 2, max: 25}],
    message: messages.errors.validation.incorrectLength(2, 25),
  },
  {
    field: 'email',
    method: FormValidator.isEmpty,
    validWhen: false,
    message: messages.errors.validation.entityIsRequired,
  },
  {
    field: 'email',
    method: 'isEmail',
    validWhen: true,
    message: messages.errors.validation.invalidValue('Email'),
  },
  {
    field: 'Email',
    method: FormValidator.isEmpty,
    validWhen: false,
    message: messages.errors.validation.entityIsRequired,
  },
  {
    field: 'password',
    method: FormValidator.isEmpty,
    validWhen: false,
    message: messages.errors.validation.entityIsRequired,
  },
  {
    field: 'password',
    method: 'matches',
    validWhen: true,
    args: [/^[a-zA-Z0-9]+$/],
    message: messages.errors.validation.incorrectPattern('a-z, A-Z, 0-9'),
  },
  {
    field: 'password',
    method: 'isLength',
    validWhen: true,
    args: [{min: 7, max: 30}],
    message: messages.errors.validation.incorrectLength(7, 30),
  },
  {
    field: 'newPassword',
    method: FormValidator.isEmpty,
    validWhen: false,
    message: messages.errors.validation.entityIsRequired,
  },
  {
    field: 'newPassword',
    method: 'matches',
    validWhen: true,
    args: [/^[a-zA-Z0-9]+$/],
    message: messages.errors.validation.incorrectPattern('a-z, A-Z, 0-9'),
  },
  {
    field: 'newPassword',
    method: 'isLength',
    validWhen: true,
    args: [{min: 7, max: 30}],
    message: messages.errors.validation.incorrectLength(7, 30),
  },
  {
    field: 'telephone',
    method: 'matches',
    args: [/^\+380\d{3}\d{2}\d{2}\d{2}$/],
    validWhen: true,
    message: messages.errors.validation.invalidValue('Phone number'),
  },
  {
    field: 'isAdmin',
    method: 'isBoolean',
    validWhen: true,
    message: messages.errors.validation.incorrectType('boolean'),
  },
  {
    field: 'enabled',
    method: 'isBoolean',
    validWhen: true,
    message: messages.errors.validation.incorrectType('boolean'),
  },
];
