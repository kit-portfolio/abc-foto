import {formValidationRules} from './formValidationRules';
import {FormValidator} from './FormValidator';

/**
 * Helper function applying validation rules to object provided.
 * @param   data - object to be validated
 */
export function validateRegistrationForm(data) {
  const fields = Object.keys(data);

  const currentValidationRules = FormValidator.checkValidity(
      formValidationRules,
      fields,
  );

  const customValidator = new FormValidator(currentValidationRules);

  return customValidator.validate(data);
}
