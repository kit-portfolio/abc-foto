import Product from '../models/Product.model';
import messages from '../config/messages';

/**
 * Helper function creating query object.
 * @param orderProducts - order products array
 */
export async function verifyProductAvailability(orderProducts) {
  try {
    const productsAvailabilityDetails = await orderProducts.reduce(
        async (resultPromise, orderItem) => {
          const result = await resultPromise;
          const dbProduct = await Product.findOne({_id: orderItem.product._id});
          if (!dbProduct) return;
          const orderedQuantity = orderItem.cartQuantity;
          const realQuantity = dbProduct.quantity;
          result.push({
            productId: dbProduct._id,
            itemNo: dbProduct.code,
            orderedQuantity,
            realQuantity,
            diff: realQuantity - orderedQuantity,
            available: realQuantity >= orderedQuantity,
          });

          return result;
        },
        Promise.resolve([]),
    );

    const unavailableProductIds = productsAvailabilityDetails
        .filter((item) => !item.available)
        .map((item) => item.productId);

    const unavailableProducts = await Product.find({
      _id: {$in: unavailableProductIds},
    });

    return {
      productsAvailabilityStatus: productsAvailabilityDetails.every(
          (product) => product.available,
      ),
      productsAvailabilityDetails,
      unavailableProducts,
    };
  } catch (err) {
    return {
      message: messages.errors.generic.serverError,
    };
  }
}
