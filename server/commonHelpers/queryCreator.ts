import {isJSON} from './isJSON';
import {FormValidator} from '../validation/FormValidator';

const excludedParams = ['letterSubject', 'letterHtml'];

/**
 * Helper function creating query object.
 * @param data - source incoming
 * @returns query object
 */
export function queryCreator(data) {
  return Object.keys(data).reduce((queryObject, param) => {
    if (isJSON(data[param])) {
      queryObject[param] = JSON.parse(data[param]);
    } else if (
      !FormValidator.isEmpty(data[param]) &&
      !excludedParams.includes(param)
    ) {
      queryObject[param] = data[param];
    }

    return queryObject;
  }, {});
}
