import _ from 'lodash';
import {Request, Response} from 'express';
import {queryCreator} from '../commonHelpers/queryCreator';
import Product from '../models/Product.model';
import message, {ENTITY, OPERATION} from '../config/messages';

/**
 * Create new product
 * @route   POST /products
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function addProduct(rq: Request, rs: Response): void {
  const productFields = _.cloneDeep(rq.body);
  productFields.code = Math.random().toString(36).substring(3).toUpperCase();
  const updatedProduct = queryCreator(productFields);
  const newProduct = new Product(updatedProduct);
  newProduct
      .save()
      .then((product) => rs.status(201).json(product))
      .catch((e) =>
        rs.status(400).json({
          message: message.entityManipulation.error(ENTITY.PRODUCT, OPERATION.CREATE),
          error: e,
        }),
      );
}

/**
 * Update existing product by ID
 * @route   PUT /products/:id
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function updateProduct(rq: Request, rs: Response): void {
  Product.findOne({_id: rq.params.id})
      .then((product) => {
        if (!product) {
          return rs.status(404).json({
            message: message.errors.entitySpecific.notFound(ENTITY.PRODUCT),
          });
        } else {
          const productFields = _.cloneDeep(rq.body);
          const updatedProduct = queryCreator(productFields);
          Product.findOneAndUpdate(
              {_id: rq.params.id},
              {$set: updatedProduct},
              {new: true},
          )
              .then((product) => rs.status(200).json(product))
              .catch((e) =>
                rs.status(400).json({
                  message: message.entityManipulation.error(ENTITY.PRODUCT, OPERATION.UPDATE),
                  error: e,
                }),
              );
        }
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Get existing products
 * @route   GET /products
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getProducts(rq: Request, rs: Response): void {
  Product.find()
      .then((product) => rs.json(product))
      .catch((e) =>
        rs.status(400).json({
          message: message.entityManipulation.error(ENTITY.PRODUCT, OPERATION.READ),
          error: e,
        }),
      );
}

/**
 * Get existing product by id
 * @route   GET /products/:id
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getProductById(rq: Request, rs: Response): void {
  Product.findById(rq.params.id)
      .then((product) => {
        if (!product) {
          rs.status(404).json({
            message: message.errors.entitySpecific.notFound(ENTITY.PRODUCT),
          });
        } else {
          rs.json(product);
        }
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}
