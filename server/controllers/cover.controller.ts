import _ from 'lodash';
import {Request, Response} from 'express';
import Cover from '../models/Cover.model';
import message, {ENTITY, OPERATION} from '../config/messages';


/**
 * Place new slide to cover
 * @route   POST /cover
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export async function addNewSlide(rq: Request, rs: Response): Promise<void> { // TODO:WTF?
  if (rq.body.href) {
    await Cover.findOne({href: rq.body.href})
        .then((item) => {
          if (item) {
            rs.status(400).json({
              message: message.errors.entitySpecific.alreadyExists(ENTITY.SLIDE),
            });
          }
        })
        .catch((e) =>
          rs.status(400).json({
            message: message.errors.generic.serverError,
            error: e,
          }),
        );
  }
  const cover = new Cover(rq.body);
  cover
      .save()
      .then((product) => rs.status(201).json(product))
      .catch((e) =>
        rs.status(400).json({
          message: message.entityManipulation.error(ENTITY.SLIDE, OPERATION.CREATE),
          error: e,
        }),
      );
}


/**
 * Get all slide of cover
 * @route   GET /cover
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getSlides(rq: Request, rs: Response): void {
  Cover.find()
      .then((result) => rs.status(200).json(result))
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Delete slide by id
 * @route   DELETE /cover/:id
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function deleteSlide(rq: Request, rs: Response): void {
  // Todo Implement 404, rework all delete
  Cover.findByIdAndRemove(rq.params.id)
      .then(() =>
        rs.status(204).json({
          message: message.entityManipulation.success(ENTITY.SLIDE, OPERATION.DELETE),
        }),
      )
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

