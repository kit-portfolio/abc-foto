import {Request, Response} from 'express';
import Contact from '../models/Contact.model';
import message, {ENTITY, OPERATION} from '../config/messages';

/**
 * Create new contact
 * @route   POST /contacts
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function createContact(rq: Request, rs: Response): void {
  Contact.findOne({href: rq.body.href})
      .then((contact) => {
        if (contact) {
          if (contact.href === rq.body.href) {
            return rs
                .status(400)
                .json({
                  message: message.errors.entitySpecific.alreadyExists(ENTITY.CONTACT),
                });
          }
        }

        const newContact = new Contact(rq.body);

        newContact
            .save()
            .then((contact) => rs.status(201).json(contact))
            .catch((e) =>
              rs.status(400).json({
                message: message.entityManipulation.error(ENTITY.CONTACT, OPERATION.CREATE),
                error: e,
              }),
            );
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Get all contacts
 * @route   GET /contacts
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getContacts(rq: Request, rs: Response): void {
  Contact.find()
      .then((contacts) => {
        if (rq.query.enabledOnly) {
          rs.status(200).json(contacts.filter((item) => item.enabled));
        } else rs.status(200).json(contacts);
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Get contact by ID
 * @route   GET /contacts/:id
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getContactById(rq: Request, rs: Response): void {
  Contact.findOne({_id: rq.params.id})
      .then((contact) => {
        if (contact) rs.status(200).json(contact);
        else {
          rs.status(404).json({
            message: message.errors.entitySpecific.notFound(ENTITY.CONTACT),
          });
        }
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Update contact by ID
 * @route   PUT /contacts/:id
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function updateContact(rq: Request, rs: Response): void {
  Contact.findOne({_id: rq.params.id})
      .then((contact) => {
        if (!contact) {
          return rs.status(404).json({
            message: message.errors.entitySpecific.notFound(ENTITY.CONTACT),
          });
        } else {
          Contact.findOneAndUpdate(
              {_id: rq.params.id},
              {$set: rq.body},
              {new: true},
          )
              .then((contact) => rs.json(contact))
              .catch((e) =>
                rs.status(400).json({
                  message: message.entityManipulation.error(ENTITY.CONTACT, OPERATION.UPDATE),
                  error: e,
                }),
              );
        }
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Delete contact by ID
 * @route   DELETE /contacts/:id
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function deleteContact(rq: Request, rs: Response): void {
  Contact.findOne({_id: rq.params.id})
      .then((contact) => {
        if (!contact) {
          return rs.status(404).json({
            message: message.errors.entitySpecific.notFound(ENTITY.CONTACT),
          });
        } else {
          Contact.deleteOne({_id: rq.params.id})
              .then(() =>
                rs.status(204).json({
                  message: message.entityManipulation.success(ENTITY.CONTACT, OPERATION.DELETE),
                }),
              )
              .catch((e) =>
                rs.status(400).json({
                  message: message.errors.generic.serverError,
                  error: e,
                }),
              );
        }
      });
}
