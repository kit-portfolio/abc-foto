import _ from 'lodash';
import {Request, Response} from 'express';
import {queryCreator} from '../commonHelpers/queryCreator';
import {validateRegistrationForm} from '../validation/validationHelper';
import Subscription from '../models/Subscription.model';
import message, {ENTITY, OPERATION} from '../config/messages';

/**
 * Create new subscription
 * @route   POST /subscription
 * @access  Public
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function createSubscription(rq: Request, rs: Response): void {
  const initialQuery = _.cloneDeep(rq.body);

  const {errors, isValid} = validateRegistrationForm(rq.body);
  if (!isValid) {
    rs.status(400).json(errors);
    return;
  }

  Subscription.findOne({email: rq.body.email})
      .then((subscription) => {
        if (subscription) {
          if (subscription.email === rq.body.email) {
            return rs
                .status(400)
                .json({
                  message: message.errors.entitySpecific.alreadyExists(ENTITY.SUBSCRIPTION),
                });
          }
        }

        const newSubscription = new Subscription(queryCreator(initialQuery));

        newSubscription
            .save()
            .then((subscription) => rs.status(201).json(subscription))
            .catch((e) =>
              rs.status(400).json({
                message: message.entityManipulation.error(ENTITY.SUBSCRIPTION, OPERATION.CREATE),
                error: e,
              }),
            );
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}
