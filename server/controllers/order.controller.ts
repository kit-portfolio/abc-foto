import _ from 'lodash';
import {Request, Response} from 'express';
import Order from '../models/Order.model';
import User from '../models/User.model';
import message, {ENTITY, OPERATION} from '../config/messages';

/**
 * Helper function intended to generate order ID of proper shape
 */
function createId(): string {
  const id = Math.random().toString(36).substring(3).toUpperCase().split('');
  id[3] = '-';
  return id.join('');
}

/**
 * Place Order
 * @route   POST /orders
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export async function addNewOrder(rq: Request, rs: Response): Promise<void> { // TODO:WTF?
  const orderFields = _.cloneDeep(rq.body);
  try {
    if (rq.body.client.userId) {
      await User.findById({_id: rq.body.client.userId})
          .then((user) => {
            if (!user) {
              throw new Error(message.errors.entitySpecific.notFound(ENTITY.ORDER));
            }
          });
    }
    orderFields.orderId = createId();
    const newOrder = new Order(orderFields);

    newOrder
        .save()
        .then((product) => rs.status(201).json(product))
        .catch((e) =>
          rs.status(400).json({
            message: message.entityManipulation.error(ENTITY.ORDER, OPERATION.CREATE),
            error: e,
          }),
        );
  } catch (e) {
    rs.status(400).json({
      message: message.errors.generic.serverError,
      error: e,
    });
  }
}

/**
 * Update order by ID
 * @route   PUT /orders/:id
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function updateOrder(rq: Request, rs: Response): void {
  Order.findOne({_id: rq.params.id})
      .then((order) => {
        if (!order) {
          return rs.status(404).json({
            message: message.errors.entitySpecific.notFound(ENTITY.ORDER),
          });
        } else {
          const orderFields = _.cloneDeep(rq.body);
          Order.findOneAndUpdate(
              {_id: rq.params.id},
              {$set: orderFields},
              {new: true},
          )
              .then((order) => rs.json(order))
              .catch((e) =>
                rs.status(400).json({
                  message: message.entityManipulation.error(ENTITY.ORDER, OPERATION.UPDATE),
                  error: e,
                }),
              );
        }
      })
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Get all orders
 * @route   GET /orders
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getOrders(rq: Request, rs: Response): void {
  Order.find()
      .then((order) => rs.status(200).json(order))
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Get order by user ID
 * @route   GET /orders/:orderNo
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function getOrderByUserId(rq: Request, rs: Response): void {
  Order.find({'userId': rq.params.userId})
      .then((order) => rs.status(200).json(order))
      .catch((e) =>
        rs.status(400).json({
          message: message.errors.generic.serverError,
          error: e,
        }),
      );
}

/**
 * Delete order by ID
 * @route   DELETE /orders/:id
 * @access  Private
 * @param   rq - HTTP request
 * @param   rs - HTTP response
 */
export function deleteOrder(rq: Request, rs: Response): void {
  Order.findOne({_id: rq.params.id})
      .then((order) => {
        if (!order) {
          return rs.status(404).json({
            message: message.errors.entitySpecific.notFound(ENTITY.ORDER),
          });
        } else {
          Order.deleteOne({_id: rq.params.id})
              .then(() =>
                rs.status(204).json({
                  message: message.entityManipulation.success(ENTITY.ORDER, OPERATION.DELETE),
                }),
              )
              .catch((e) =>
                rs.status(400).json({
                  message: message.errors.generic.serverError,
                  error: e,
                }),
              );
        }
      });
}
