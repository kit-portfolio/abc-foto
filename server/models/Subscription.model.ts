import {Schema, Document, model} from 'mongoose';
import {MODEL} from '../config/constants';

interface ISubscription extends Document {
  email: string,
}

const SubscriptionSchema: Schema = new Schema(
    {
      email: {
        type: String,
        required: true,
      },
    },
    {strict: false},
);

export default model<ISubscription>(MODEL.SUBSCRIPTION, SubscriptionSchema);
