import {Schema, Document, model} from 'mongoose';
import {MODEL} from '../config/constants';

interface IProduct extends Document {
  name: string,
  manufacturer: string,
  images: string,
  description: string,
  quantity: number,
  type: string,
  category: string,
  code: string,
  price: string,
  accessories?: Array<string>,
  characteristics: Array<ICharacteristic>,
  sale?: ISale,
}

interface ICharacteristic {
  title: string,
  value: string,
  isFilterable: boolean,
}

interface ISale {
  discount: number,
  endDate: string,
}

const ProductSchema: Schema = new Schema(
    {
      name: {
        type: String,
        required: true,
      },
      manufacturer: {
        type: String,
        required: true,
      },
      images: {
        type: [{
          image: String,
          preview: String,
        }],
        required: true,
      },
      description: {
        type: String,
        required: true,
      },
      quantity: {
        type: Number,
        required: true,
        default: 0,
      },
      type: {
        type: String,
        required: true,
      },
      category: {
        type: String,
        required: true,
      },
      code: {
        type: String,
        required: true,
      },
      price: {
        type: String,
        required: true,
      },
      accessories: {
        type: [String],
      },
      characteristics: {
        type: [{
          title: String,
          value: String,
          isFilterable: Boolean,
        }],
        required: true,
      },
      sale: {
        type: {
          discount: Number,
          endDate: String,
        },
      },
    },
    {strict: false},
);

ProductSchema.index({'$**': 'text'});

export default model<IProduct>(MODEL.PRODUCT, ProductSchema);
