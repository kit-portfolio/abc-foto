import {Schema, Document, model} from 'mongoose';
import {MODEL} from '../config/constants';

export interface ICover extends Document {
  link: string,
  href: string,
  alt: string,
}

const Cover: Schema = new Schema(
    {
      link: {
        type: String,
        required: true,
      },
      href: {
        type: String,
        required: true,
      },
      alt: {
        type: String, // TODO: И что нам с этим делать?)
        required: true,
      },
    },
    {strict: false},
);

export default model<ICover>(MODEL.COVER, Cover);
