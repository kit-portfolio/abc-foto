import {Schema, Document, model} from 'mongoose';
import {MODEL} from '../config/constants';

export interface IUser extends Document {
  firstName: string,
  lastName?: string,
  email: string,
  password: string,
  phone: string,
  birthDate?: string,
  gender?: GENDER,
  avatarUrl?: string,
  isAdmin: boolean,
  enabled: boolean,
  registrationDate: boolean,
}

enum GENDER {
  MALE = 'male',
  FEMALE ='female',
}

const UserSchema: Schema = new Schema(
    {
      firstName: {
        type: String,
        required: true,
      },
      lastName: {
        type: String,
      },
      email: {
        type: String,
        required: true,
      },
      password: {
        type: String,
        required: true,
      },
      phone: {
        type: String,
        required: true,
      },
      birthDate: {
        type: Date,
      },
      gender: {
        type: String,
        enum: [GENDER.MALE, GENDER.FEMALE],
      },
      avatarUrl: {
        type: String,
      },
      isAdmin: {
        type: Boolean,
        required: true,
        default: false,
      },
      enabled: {
        type: Boolean,
        required: true,
        default: true,
      },
      registrationDate: {
        type: Date,
        default: Date.now,
      },
    },
    {strict: false},
);

export default model<IUser>(MODEL.USER, UserSchema);
