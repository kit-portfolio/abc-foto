import {Schema, Document, model} from 'mongoose';
import {MODEL} from '../config/constants';

interface IContact extends Document {
  label: string,
  image: string,
  href: string,
  enabled: boolean
}

const ContactSchema: Schema = new Schema(
    {
      label: {
        type: String,
        required: true,
      },
      image: {
        type: String,
        required: true,
      },
      href: {
        type: String,
        required: true,
      },
      enabled: {
        type: Boolean,
        default: true,
        required: true,
      },
    },
    {strict: false},
);

export default model<IContact>(MODEL.CONTACT, ContactSchema);
